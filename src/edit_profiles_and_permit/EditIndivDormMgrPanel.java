package edit_profiles_and_permit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import indiv_entities_for_panels.IndivDormMgr;
import jhunn_ate_visualManager.Utilities;

public class EditIndivDormMgrPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivDormMgr dormMgrGuide;
	private Utilities util;
	private JButton saveJB;
	public EditIndivDormMgrPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		dormMgrGuide  = new IndivDormMgr();
		dormMgrGuide.setLabels(this);
		dormMgrGuide.setButtons(this);
		dormMgrGuide.setTextFields(this);
		addButtons();
		TFHandler handler = new TFHandler();
		addTFListener(handler);
	}
	public void addButtons(){
		saveJB = new JButton("Save changes!");
		util.buttonProperties(saveJB, 450, 450, 100, 50, this);
	}
	public void addListener(ActionListener listen){
		dormMgrGuide.getBack().addActionListener(listen);
		saveJB.addActionListener(listen);
	}
	public void addTFListener(ActionListener listen){
		dormMgrGuide.addTFListener(listen);

	}
	public JButton getBack(){ return dormMgrGuide.getBack();}
	public JButton getSave(){ return saveJB;}
	
	
	public String getNameStr(){ return dormMgrGuide.getNameStr();}
	public String getBdayStr(){ return dormMgrGuide.getBdayStr();}
	public String getAddressStr(){ return dormMgrGuide.getAddressStr();}
	public String getContactNumStr(){ return dormMgrGuide.getContactNumStr();}
	public String getStatusStr(){ return dormMgrGuide.getStatusStr();}
	public String getUsernameStr(){ return dormMgrGuide.getUsernameStr();}
	public String getPasswordStr(){ return dormMgrGuide.getPasswordStr();}
	
	public void setTFToNull(){
		dormMgrGuide.getNameTF().setText(null);
		dormMgrGuide.getbdayTF().setText(null);
		dormMgrGuide.getAddressTF().setText(null);
		dormMgrGuide.getContactNoTF().setText(null);
		dormMgrGuide.getStatusTF().setText(null);
		dormMgrGuide.getUserNameTF().setText(null);
		dormMgrGuide.getPasswordTF().setText(null);
		
	}
	private class TFHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == dormMgrGuide.getNameTF()){
				errorTrapTF(dormMgrGuide.getNameTF(), 2);
			}else if(e.getSource() == dormMgrGuide.getbdayTF()){
				errorTrapTF(dormMgrGuide.getbdayTF(), 3);
			}else if(e.getSource() == dormMgrGuide.getAddressTF()){
				errorTrapTF(dormMgrGuide.getAddressTF(), 2);
			}else if(e.getSource() == dormMgrGuide.getContactNoTF()){
				errorTrapTF(dormMgrGuide.getContactNoTF(), 1);
			}else if(e.getSource() == dormMgrGuide.getStatusTF()){
				errorTrapTF(dormMgrGuide.getStatusTF(), 2);
			}else if(e.getSource() == dormMgrGuide.getUserNameTF()){
				errorTrapTF(dormMgrGuide.getUserNameTF(), 3);
			}else if(e.getSource() == dormMgrGuide.getPasswordTF()){
				errorTrapTF(dormMgrGuide.getPasswordTF(), 3);
			}
			
		}
		
	}
	public void errorTrapTF(JTextField field, int type){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
}
