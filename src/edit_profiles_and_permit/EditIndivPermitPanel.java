package edit_profiles_and_permit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import indiv_entities_for_panels.IndivPermit;
import jhunn_ate_visualManager.Utilities;

public class EditIndivPermitPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivPermit permitGuide;
	private Utilities util;
	private JButton saveJB;
	public EditIndivPermitPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		permitGuide = new IndivPermit();
		util = new Utilities();
		permitGuide.setLabels(this);
		permitGuide.setTextFields(this);
		permitGuide.setButtons(this);
		addButtons();
		TFHandler handMe = new TFHandler();
		setTFHandler(handMe);
	}
	public void addButtons(){
		
		saveJB = new JButton("Submit!");
		util.buttonProperties(saveJB, 550, 550, 100, 100, this);
	}
	public void addListener(ActionListener listen){
		permitGuide.getBack().addActionListener(listen);
		saveJB.addActionListener(listen);
	}
	public void setTFHandler(ActionListener listen){
		permitGuide.setTFHandler(listen);
	}
	public void setTFToNull(){
		permitGuide.setTFToNull();
	}
	public String getNameStr(){ return permitGuide.getNameStr();}
	public String getCategoryStr(){ return permitGuide.getCategoryStr();}
	public String getReasonStr(){ return permitGuide.getReasonStr();}
	public String getPlaceStr(){ return permitGuide.getPlaceStr();}
	
	private class TFHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == permitGuide.getNameTF()){
				errorTrapTF(permitGuide.getNameTF() , 2);
			}else if(e.getSource() == permitGuide.getCategoryTF()){
				errorTrapTF(permitGuide.getCategoryTF() , 2);
			}else if(e.getSource() == permitGuide.getReasonTF()){
				errorTrapTF( permitGuide.getReasonTF() , 3);
			}else if(e.getSource() == permitGuide.getPlaceTF()){
				errorTrapTF(permitGuide.getPlaceTF(), 3);
			}
		}
		
	}
	public JButton getBack(){return permitGuide.getBack();}
	public JButton getSave(){return saveJB;}
	public void errorTrapTF(JTextField field, int type){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
}
