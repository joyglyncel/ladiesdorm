package edit_profiles_and_permit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import indiv_entities_for_panels.IndivGuard;
import jhunn_ate_visualManager.Utilities;

public class EditIndivGuardPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivGuard guardGuide;
	private JButton saveJB;
	private Utilities util;
	public EditIndivGuardPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		guardGuide = new IndivGuard();
		guardGuide.setLabels(this);
		guardGuide.setButtons(this);
		guardGuide.setTextFields(this);
		addButtons();
		TFHandler hand = new TFHandler();
		addListener(hand);
	}
	public void addButtons(){
		saveJB = new JButton("Save changes!");
		util.buttonProperties(saveJB, 450, 450, 100, 50, this);
	}
	public void addListener(ActionListener listen){
		guardGuide.getBack().addActionListener(listen);
		saveJB.addActionListener(listen);
	}
	public void addTFListener(ActionListener listen){
		guardGuide.setTFListener(listen);
	}
	public JButton getBack(){ return guardGuide.getBack();}
	public JButton getSaveJB(){ return saveJB;}
	public void setTFToNull(){
		guardGuide.setTFToNull();
	}
	public String getNameStr(){ return guardGuide.getNameStr();}
	public String getBdayStr(){ return guardGuide.getBdayStr();}
	public String getAddressStr(){ return guardGuide.getAddressStr();}
	public String getContactNoStr(){ return guardGuide.getContactNoStr();}
	public String getStatusStr(){ return guardGuide.getStatusStr();}
	public String getUsernameStr(){ return guardGuide.getUsernameStr();}
	public String getPasswordStr(){ return guardGuide.getPasswordStr();}
	public String getEmployerStr(){ return guardGuide.getEmployerStr();}
	public String getIDNumStr(){ return guardGuide.getIDNumStr();}
	public String getDutyTimeStr(){ return guardGuide.getDutyTimeStr();}
	
	private class TFHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == guardGuide.getStatusTF()){
				errorTrapTF(guardGuide.getStatusTF(), 2);
			}else if(e.getSource() == guardGuide.getNameTF()){
				errorTrapTF(guardGuide.getNameTF(), 2);
			}else if(e.getSource() == guardGuide.getBdayTF()){
				errorTrapTF(guardGuide.getBdayTF(), 3);
			}else if(e.getSource() == guardGuide.getAddressTF()){
				errorTrapTF(guardGuide.getAddressTF(), 3);
			}else if(e.getSource() == guardGuide.getContactNoTF()){
				errorTrapTF(guardGuide.getContactNoTF(), 1);
			}else if(e.getSource() == guardGuide.getUserNameTF()){
				errorTrapTF(guardGuide.getUserNameTF(), 3);
			}else if(e.getSource() == guardGuide.getPasswordTF()){
				errorTrapTF(guardGuide.getPasswordTF(), 3);
			}else if(e.getSource() == guardGuide.getEmployerTF()){
				errorTrapTF(guardGuide.getEmployerTF(), 2);
			}else if(e.getSource() == guardGuide.getidNumberTF()){
				errorTrapTF(guardGuide.getidNumberTF(), 3);
			}else if(e.getSource() == guardGuide.getDutyTimeTF()){
				errorTrapTF(guardGuide.getDutyTimeTF(), 3);
			}
		}
		
	}
	public void errorTrapTF(JTextField field, int type){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
}
