package edit_profiles_and_permit;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import indiv_entities_for_panels.IndivStudent;
import jhunn_ate_visualManager.Utilities;

public class EditIndivStudentPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivStudent studentGuide;
	private Utilities util;
	private JButton saveJB;
	public EditIndivStudentPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		studentGuide = new IndivStudent();
		studentGuide.setLabels(this);
		studentGuide.setTextFields(this);
		studentGuide.setButtons(this);
		addButtons();
		TFHandler hand = new TFHandler();
		setTFHandler(hand);
	}
	public void addButtons(){
		
		saveJB = new JButton("Save changes!");
		util.buttonProperties(saveJB, 550, 550, 100, 100, this);
	}
	public void setTFToNull(){
		studentGuide.setTFToNull();
	}
	public void addListener(ActionListener listen){
		studentGuide.getBack().addActionListener(listen);
		saveJB.addActionListener(listen);
	}
	public void setTFHandler(ActionListener listen){
		studentGuide.setTFHandler(listen);
	}
	public String getMotherNameStr(){ return studentGuide.getMotherNameStr();}
	public String getMotherOccupationStr(){ return studentGuide.getMotherOccupationStr();}
	public String getMotherContactNoStr(){ return studentGuide.getMotherContactNoStr();}
	public String getMotherAddressStr(){ return studentGuide.getMotherAddressStr();}
	
	public String getNameStr(){ return studentGuide.getNameStr();}
	public String getCourseStr(){ return studentGuide.getCourseStr();}
	public String getStudentNoStr(){ return studentGuide.getStudentNoStr();}
	public String getBdayStr(){ return studentGuide.getBdayStr();}
	public String getContactNoStr(){ return studentGuide.getContactNoStr();}
	public String getSTSBracketStr(){ return studentGuide.getSTSBracketStr();}
	public String getSemStartedStr(){ return studentGuide.getSemStartedStr();}
	public String getSemEndedStr(){ return studentGuide.getSemEndedStr();}
	public String getAyStartedStr(){ return studentGuide. getAyStartedStr();}
	public String getAyendedStr(){ return studentGuide.getAyendedStr();}
	public String getHomeAddStr(){ return studentGuide.getHomeAddStr();}
	public String getEmailAddStr(){ return studentGuide.getEmailAddStr();}
	
	public String getFatherNameStr(){ return studentGuide.getFatherNameStr();}
	public String getFatherOccupationStr(){ return studentGuide.getFatherOccupationStr();}
	public String getFatherContactNoStr(){ return studentGuide.getFatherContactNoStr();}
	public String getFatherAddressStr(){ return studentGuide.getFatherAddressStr();}
	
	public String getGuardianNameStr(){ return studentGuide.getGuardianNameStr();}
	public String getGuardianOccupationStr(){ return studentGuide.getGuardianOccupationStr();}
	public String getGuardianContactNoStr(){ return studentGuide.getGuardianContactNoStr();}
	public String getGuardianAddressStr(){ return studentGuide.getGuardianAddressStr();}
	public String getRelationshipToGuardianStr(){ return studentGuide.getRelationshipToGuardianStr();}
	
	
	
	public JButton getBack(){return studentGuide.getBack();}
	public JButton getSave(){return saveJB;}
	
	private class TFHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == studentGuide.getNameTF()){
				errorTrapTF(studentGuide.getNameTF(), 2);
			}else if(e.getSource() == studentGuide.getHomeAddressTF()){
				errorTrapTF(studentGuide.getHomeAddressTF(), 3);
			}else if(e.getSource() == studentGuide.getRoomNumberTF()){
				errorTrapTF(studentGuide.getRoomNumberTF(), 1);
			}else if(e.getSource() == studentGuide.getMotherNameTF()){
				errorTrapTF(studentGuide.getMotherNameTF(), 2);
			}else if(e.getSource() == studentGuide.getMotherOccupationTF()){
				errorTrapTF(studentGuide.getMotherOccupationTF(), 2);
			}else if(e.getSource() == studentGuide.getMotherContactNoTF()){
				errorTrapTF(studentGuide.getMotherContactNoTF(), 1);
			}else if(e.getSource() == studentGuide.getMotherAddressTF()){
				errorTrapTF(studentGuide.getMotherAddressTF(), 3);
			}else if(e.getSource() == studentGuide.getFatherNameTF()){
				errorTrapTF(studentGuide.getFatherNameTF(), 2);
			}else if(e.getSource() == studentGuide.getFatherOccupationTF()){
				errorTrapTF(studentGuide.getFatherOccupationTF(), 2);
			}else if(e.getSource() == studentGuide.getFatherContactNoTF()){
				errorTrapTF(studentGuide.getFatherContactNoTF(), 1);
			}else if(e.getSource() == studentGuide.getFatherAddressTF()){
				errorTrapTF(studentGuide.getFatherAddressTF(), 3);
			}else if(e.getSource() == studentGuide.getGuardianNameTF()){
				errorTrapTF(studentGuide.getGuardianNameTF(), 2);
			}else if(e.getSource() == studentGuide.getGuardianOccupationTF()){
				errorTrapTF(studentGuide.getGuardianOccupationTF(), 2);
			}else if(e.getSource() == studentGuide.getGuardianContactNoTF()){
				errorTrapTF(studentGuide.getGuardianContactNoTF(), 1);
			}else if(e.getSource() == studentGuide.getGuardianAddressTF()){
				errorTrapTF(studentGuide.getGuardianAddressTF(), 3);
			}else if(e.getSource() == studentGuide.getRelationshipToGuaridanTF()){
				errorTrapTF(studentGuide.getRelationshipToGuaridanTF(), 2);
			}else if(e.getSource() == studentGuide.getUserNameTF()){
				errorTrapTF(studentGuide.getUserNameTF(), 1);
			}else if(e.getSource() == studentGuide.getPasswordTF()){
				errorTrapTF(studentGuide.getPasswordTF(), 3);
			}else if(e.getSource() == studentGuide.getCourseTF()){
				errorTrapTF(studentGuide.getCourseTF(), 2);
			}else if(e.getSource() == studentGuide.getStudentNoTF()){
				errorTrapTF(studentGuide.getStudentNoTF(), 1);
			}else if(e.getSource() == studentGuide.getContactNoTF()){
				errorTrapTF(studentGuide.getContactNoTF(), 1);
			}else if(e.getSource() == studentGuide.getSTSBracketTF()){
				errorTrapTF(studentGuide.getSTSBracketTF(), 3);
			}else if(e.getSource() == studentGuide.getSemStartedTF()){
				errorTrapTF(studentGuide.getSemStartedTF(), 3);
			}else if(e.getSource() == studentGuide.getSemEndedTF()){
				errorTrapTF(studentGuide.getSemEndedTF(), 3);
			}else if(e.getSource() == studentGuide.getAYStartedTF()){
				errorTrapTF(studentGuide.getAYStartedTF(), 3);
			}else if(e.getSource() == studentGuide.getAYEndedTF()){
				errorTrapTF(studentGuide.getAYEndedTF(), 3);
			}else if(e.getSource() == studentGuide.getBdayTF()){
				errorTrapTF(studentGuide.getBdayTF(), 3);
			}	
		}	
	}
	public void errorTrapTF(JTextField field, int type){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
}
