package view_profiles;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import indiv_entities_for_panels.IndivStudent;

public class ViewIndivStudentPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivStudent student;
	public ViewIndivStudentPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		student = new IndivStudent();
		student.setLabels(this);
		student.setButtons(this);
		student.setTextFields(this);
	}
	public void setNotEditable(){
		student.setIfEditable(false);
	}
	public void setTFListener(ActionListener listen ){
		student.setTFHandler(listen);
	}
	public void setTFToNull(){
		student.setTFToNull();
	}
	public void setButtonHandler(ActionListener listen){
		student.getBack().addActionListener(listen);
	}
	public JButton getBack(){
		return student.getBack();
	}
}
