package view_profiles;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import indiv_entities_for_panels.IndivDormMgr;
import jhunn_ate_visualManager.Utilities;

public class ViewIndivDormMgrPanel  extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivDormMgr dormMgrGuide;
	private Utilities util;
	public ViewIndivDormMgrPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		dormMgrGuide  = new IndivDormMgr();
		dormMgrGuide.setLabels(this);
		dormMgrGuide.setButtons(this);
		dormMgrGuide.setTextFields(this);
		addButtons();
	}
	public void addButtons(){
		
	}
	public void addListener(ActionListener listen){
		dormMgrGuide.getBack().addActionListener(listen);
		
	}
	public void addTFListener(ActionListener listen){
		dormMgrGuide.getNameTF().addActionListener(listen);
		dormMgrGuide.getbdayTF().addActionListener(listen);
		dormMgrGuide.getAddressTF().addActionListener(listen);
		dormMgrGuide.getContactNoTF().addActionListener(listen);
		dormMgrGuide.getStatusTF().addActionListener(listen);
		dormMgrGuide.getUserNameTF().addActionListener(listen);
		dormMgrGuide.getPasswordTF().addActionListener(listen);

	}
	public void setToNull(){
		dormMgrGuide.setTFToNull();
	}
	public void setNotEditable(){
		dormMgrGuide.setIfEditable(false);
	}
	public JButton getBack(){ return dormMgrGuide.getBack();}

	
}
