package view_profiles;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import indiv_entities_for_panels.IndivGuard;

public class ViewIndivGuardPanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private IndivGuard guard;
	public ViewIndivGuardPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		guard = new IndivGuard();
		guard.setLabels(this);
		guard.setButtons(this);
		guard.setTextFields(this);
	}
	public void setNotEditable(){
		guard.setIfEditable(false);
	}
	public void setButtonListener(ActionListener listen ){
		guard.setListener(listen);
	}
	public void setTFToNUll(){
		guard.setTFToNull();
	}
	public JButton getBack(){
		return guard.getBack();
	}
}
