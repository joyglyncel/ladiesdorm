package sign_up_panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class SignUpDormManagerPanel extends JPanel {
	/**
	 * pag makasign-up mabalik ha log-in panel
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private JButton createJB;
	private Utilities util;
	private JLabel categoryJL;
	private String category;
	public SignUpDormManagerPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		back = new JButton("Back");
		createJB = new JButton("Connect!");
		util.buttonProperties(back, 100, 400, 100, 100, this);
		util.buttonProperties(createJB, 100, 80, 100, 100, this);
		categoryJL = new JLabel();
		util.labelProperties(categoryJL, 250, 150, 500, 100, "Dorm Manager", this);
		category = new String("Dorm Manager");
	}
	public String getCategory(){ return category;}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		createJB.addActionListener(listen);
	}
	public JButton getBack(){ return back;}
	public JButton getCreateJB(){ return createJB;}
	
}
