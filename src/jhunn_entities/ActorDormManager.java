package jhunn_entities;

public class ActorDormManager {
	private String name;
	private String birthday;
	private String address;
	private String contact_number;
	private String status;
	private String userName;
	private String password;
	private String id_number;
	public ActorDormManager(){
	//	id_number,name,birthday,address,contact_number,statuss(Single, Married, Widowed)
		name = new String();
		birthday = new String();
		address = new String();
		contact_number = new String();
		status = new String();
		userName= new String();
		password= new String();
		id_number= new String();
	}
	public void setID_number(String str){ id_number = str;}
	public void setName(String str){ name = str;}
	public void setBirthday(String str){ birthday = str;}
	public void setAddress(String str){ address = str; }
	public void setContactNo(String str){ contact_number = str;}
	public void setStatus(String str){ status = str;}
	public void setUserName(String str){ userName = str;}
	public void setPassword(String str){ password = str;}
	
	public String getID_number(){return id_number;}
	public String getName(){ return name; }
	public String getBirthday(){ return birthday; }
	public String getAddress(){ return address; }
	public String getContactNo(){ return contact_number; }
	public String getStatus(){ return status; }
	public String getUserName(){ return userName; }
	public String getPassword(){ return password; }
	
	
}
