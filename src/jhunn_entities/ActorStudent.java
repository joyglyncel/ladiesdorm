package jhunn_entities;

public class ActorStudent {

	private String name;//
	private String course;//
	private String student_number; //
	private String birthday; //
	private String contact_num;//
	private String sts_bracket;//
	private String semStarted;
	private String semEnded;
	private String ayStarted;
	private String ayEnded;
	private String home_add;//
	private String email_add;//
	private String room_num;//
	private String status;
	
	//parentsInfo
	private String motherName;
	private String motherOccupation;
	private String motherContactNo;
	private String motherAddress;
	
	private String fatherName;
	private String fatherOccupation;
	private String fatherContactNo;
	private String fatherAddress;
	
	//guardianInfo
	
	private String guardianName;
	private String guardianOccupation;
	private String guardianContactNo;
	private String guardianAddress;
	private String relationshipToGuardian;
	//app
	private boolean studentIn;
	private String userName;
	private String password;
	public ActorStudent(){
		studentIn = false;
	}
	
	//setters
	public void setStudentIn(boolean bol){ studentIn = bol;}
	public void setName(String str){ name = str; }
	public void setStudentNo(String str){ student_number = str; }
	public void setBirthday(String str){ birthday = str; }
	public void setContactNo(String str){ contact_num = str; }
	public void setSTSBracket(String str){ sts_bracket = str; }
	public void setSemStarted(String str){ semStarted = str; }
	public void setSemEnded(String str){ semEnded = str; }
	public void setAYStarted(String str){ ayStarted = str; }
	public void setAYEnded(String str){ ayEnded = str; }
	public void setHomeAddress(String str){ home_add = str; }
	public void setRoomNumber(String str){ room_num = str; }
	public void setStatus(String str){ status = str; }
	public void setCourse(String str){ course = str; }
	public void setEmailAdd(String str){ email_add = str; }
	public void setUserName(String str){ userName = str;}
	public void setPassword(String str){ password = str;}
	
	public void setMotherName(String str){ motherName = str; }
	public void setMotherOccupation(String str){ motherOccupation = str; }
	public void setMotherContactNo(String str){ motherContactNo = str; }
	public void setMotherAddress(String str){ motherAddress = str; }
	
	public void setFatherName(String str){ fatherName = str; }
	public void setFatherOccupation(String str){ fatherOccupation = str; }
	public void setFatherContactNo(String str){ fatherContactNo = str; }
	public void setFatherAddress(String str){ fatherAddress = str; }
	
	public void setGuardianName(String str){ guardianName = str; }
	public void setGuardianOccupation(String str){ guardianOccupation = str; }
	public void setGuardianContactNo(String str){ guardianContactNo = str; }
	public void setGuardianAddress(String str){ guardianAddress = str; }
	public void setRelationshipToGuardian(String str){ relationshipToGuardian = str;}
	
	public String getName(){ return name; }
	public String getStudentNo(){return student_number; }
	public String getBirthday(){ return birthday; }
	public String getContactNo(){ return contact_num;}
	public String getSTSBracket(){ return sts_bracket;  }
	public String getSemStarted(){ return semStarted;}
	public String getSemEnded(){ return semEnded;}
	public String getAYStarted(){ return ayStarted; }
	public String getAYEnded(){ return ayEnded; }
	public String getHomeAddress(){ return home_add;}
	public String getRoomNumber(){ return room_num;}
	public String getStatus(){ return status; }
	public String getCourse(){ return course; }
	public String getEmailAdd() { return email_add;}
	
	
	public String getMotherName(){ return motherName;  }
	public String getMotherOccupation(){ return motherOccupation;  }
	public String getMotherContactNo(){ return motherContactNo;  }
	public String getMotherAddress(){ return motherAddress; }
	
	public String getFatherName(){ return fatherName ; }
	public String getFatherOccupation(){ return fatherOccupation ;}
	public String getFatherContactNo(){ return fatherContactNo; }
	public String getFatherAddress(){ return fatherAddress; }
	
	public String getGuardianName(){ return guardianName ; }
	public String getGuardianOccupation(){ return guardianOccupation ; }
	public String getGuardianContactNo(){ return guardianContactNo;  }
	public String getGuardianAddress(){ return guardianAddress ; }
	public String getRelationshipToGuardian(){ return relationshipToGuardian;}
	
	public boolean getStudentIn(){ return studentIn;}
	public String getUserName(){ return userName;}
	public String getPassword(){ return password;}
	
	
	
	
	
	
}
