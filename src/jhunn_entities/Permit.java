package jhunn_entities;

public class Permit {
	private int permitNo;
	private String studentNo;
	private String category;
	private String reason;
	private String date;
	private String place;
	private String time;
	
	public void setPermitNo(int no){ permitNo = no;}
	public void setStudentNo(String no){ studentNo = no;}
	public void setCategory(String no){ category = no;}
	public void setReason(String no){ reason = no;}
	public void setDate(String no){ date = no;}
	public void setPlace(String no){ place = no;}
	public void setTime(String no) { time = no;}
	
	public int getPermitNo(){ return permitNo;}
	public String getStudentNo(){ return studentNo;}
	public String getCategory(){ return category;}
	public String getReason(){ return reason;}
	public String getDate(){ return date;}
	public String getPlace(){return place;}
	public String getTime(){ return time;}
}
