package jhunn_entities;

public class ActorGuard {
	private String employer;
	private String name;
	private String id_number;
	private String duty_time;
	private String contact_number;
	private String address;
	private String birthday;
	private String status;
	private String userName;
	private String password;
	public ActorGuard(){
		employer = new String();
		name = new String();
		id_number = new String();
		duty_time= new String();
		contact_number= new String();
		address= new String();
		birthday= new String();
		status= new String();
		userName= new String();
		password= new String();
		// id_number, name,address,employer, duty_time, birthday,contact_number,status(Single,Married,Widowed)	
	}
	public void setEmployer(String str){ employer = str;}
	public void setName(String str){ name = str;}
	public void setIDNumber(String str){ id_number = str;}
	public void setDutyTime(String str){ duty_time = str;}
	public void setContactNo(String str){ contact_number = str;}
	public void setAddress(String str){ address = str;}
	public void setBirthday(String str){ birthday = str;}
	public void setStatus(String str){ status = str;}
	public void setUserName(String str){ userName = str; }
	public void setPassword(String str){ password = str; }
	
	public String getEmployer(){ return employer;}
	public String getName(){ return name;}
	public String getIDNumber(){ return id_number;}
	public String getDutyTime(){ return duty_time;}
	public String getContactNo(){ return contact_number;}
	public String getAddress(){ return address;}
	public String getBirthday(){ return birthday;}
	public String getStatus(){ return status;}
	public String getUserName(){ return userName;}
	public String getPassword(){ return password;}
	
	
}
