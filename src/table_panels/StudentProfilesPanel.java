package table_panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class StudentProfilesPanel extends JPanel{
	/**
	 * This panel should be scrollable
	 * OCt 29 adi na
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private JButton addStudentJB;
	private Utilities util;
	private JLabel roomNoJL;
	private JLabel snJL;
	private JLabel nameJL;
/*	private ArrayList<JTextField> roomList;
	
	private ArrayList<JTextField> snList;
	private ArrayList<JTextField> nameList;
	private ArrayList<JButton> editList;
	private ArrayList<JButton> viewList;
	private ArrayList<JButton> removeList;*/
	private int xSNList;
	private int xNameList;
	private int xEditList;
	private int xViewList;
	private int xRemoveList;
	private int ySNList;
	private int yNameList;
	private int yEditList;
	private int yViewList;
	private int yRemoveList;
	private int xRoom;
	private int yRoom;
	private JButton viewIndivJB;
	private JButton editIndivJB;
	private JButton removeIndivJB;
	private JTable table;
	private JScrollPane scrollPane;
	//private JButton addNewJB; nean na
	public StudentProfilesPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		declarations();
		addButtons();
		addLabels();
		addTable();
		
	/*	addTextField();
		addIndivButtonAndTextField("1","2014-64813", "April Joy Padrigano");
		addIndivButtonAndTextField("1","2014-64813", "April Joy Padrigano");
		addIndivButtonAndTextField("10","2014-64813", "April Joy Padrigano");
		addIndivButtonAndTextField("10","2014-64813", "April Joy Padrigano");
		addIndivButtonAndTextField("10","2014-64813", "April Joy Padrigano");*/
	}
	public void declarations(){
		util = new Utilities();
		xRoom = 40;
		xSNList = xRoom +100;
		xNameList= xSNList+150;
		xViewList = xNameList + 230;
		xEditList = xViewList +150;
		xRemoveList = xEditList +150;
		yRoom = 100;
		ySNList = yRoom;
		yNameList = ySNList;
		yEditList = ySNList;
		yViewList = ySNList;
		yRemoveList = ySNList;
		
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		addStudentJB.addActionListener(listen);
		viewIndivJB.addActionListener(listen);
		editIndivJB.addActionListener(listen);
		removeIndivJB.addActionListener(listen);
	}
	public void addButtons(){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 5, 50, 25, this);
		addStudentJB = new JButton("Add Student");
		util.buttonProperties(addStudentJB, 450, 30, 150, 50, this);
		viewIndivJB = new JButton("View Profile");
		editIndivJB = new JButton("Edit Profile");
		removeIndivJB = new JButton("Remove Profile");
		util.buttonProperties(viewIndivJB, 650, 70, 100, 50, this);
		util.buttonProperties(editIndivJB, 650, 150, 100, 50, this);
		util.buttonProperties(removeIndivJB, 650, 210, 100, 50, this);
//		editList = new ArrayList<JButton>();
//		viewList = new ArrayList<JButton>();
//		removeList = new ArrayList<JButton>();	
	}
/*	public void addTextField(){
		snList = new ArrayList<JTextField>();
		nameList = new ArrayList<JTextField>();
		roomList = new ArrayList<JTextField>();
	}*/
	public void addLabels(){
		roomNoJL = new JLabel();
		snJL = new JLabel();
		nameJL = new JLabel();
		util.labelProperties(roomNoJL, 50, 50, 100, 25, "Room", this);
		util.labelProperties(snJL, 150, 50, 250, 20, "Student Number", this);
		util.labelProperties(nameJL, 350, 50, 100, 25, "Name", this);
	}
	/*public void addIndivButtonAndTextField(String room,String sn, String name){
		roomList.add(new JTextField());
		util.textFieldProperties(roomList.get(roomList.size()-1), xRoom, yRoom, 50, 30, this);
		snList.add(new JTextField());
		util.textFieldProperties(snList.get(snList.size()-1), xSNList, ySNList, 100, 30, this);
		nameList.add(new JTextField());
		util.textFieldProperties(nameList.get(nameList.size()-1), xNameList, yNameList, 180, 30, this);
		viewList.add(new JButton("View"));
		util.buttonProperties(viewList.get(viewList.size()-1), xViewList, yViewList, 120, 30, this);
		editList.add(new JButton("Edit"));
		util.buttonProperties(editList.get(editList.size()-1), xEditList, yEditList, 120, 30, this);
		removeList.add(new JButton("Remove"));
		util.buttonProperties(removeList.get(removeList.size()-1), xRemoveList, yRemoveList, 120, 30, this);
		
		//setting of text & listeners
		roomList.get(roomList.size()-1).setText(room);
		nameList.get(nameList.size()-1).setText(name);
		snList.get(nameList.size()-1).setText(sn);
		
		//viewList.get(viewList.size()-1).addActionListener(listen);
		//editList.get(editList.size()-1).addActionListener(listen);
		//removeList.get(removeList.size()-1).addActionListener(listen);
		
		//set editable=false
		roomList.get(roomList.size()-1).setEditable(false);
		nameList.get(nameList.size()-1).setEditable(false);
		snList.get(snList.size()-1).setEditable(false);
		
		yRoom += 40;
		ySNList =yRoom;
		yNameList = ySNList;
		yEditList = ySNList;
		yViewList = ySNList;
		yRemoveList = ySNList;
	}*/
	
	public void addTable(){
		String[] columnNames = {"First Name",
                "Last Name",
                "Sport",
                "# of Years",
                "Vegetarian"};
		Object[][] data = {
			{"Kathy", "Smith",
			"Snowboarding", new Integer(5), new Boolean(false)},
			{"John", "Doe",
			"Rowing", new Integer(3), new Boolean(true)},
			{"Sue", "Black",
			"Knitting", new Integer(2), new Boolean(false)},
			{"Jane", "White",
			"Speed reading", new Integer(20), new Boolean(true)},
			{"Joe", "Brown",
			"Pool", new Integer(10), new Boolean(false)}
		};
		
		JTable table = new JTable(data, columnNames);
		
		scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
	}

	
	public JButton getBack(){return back;}
	public JButton getAddStudentJB(){return addStudentJB;}
	public JButton getViewIndivJB(){ return viewIndivJB;}
	public JButton getEditIndivJB(){return editIndivJB;}
	public JButton getRemoveINdivJB(){return removeIndivJB;}
	public JTable getTable(){return table;}
	public JScrollPane getScrollPane(){return scrollPane;}
	
}
