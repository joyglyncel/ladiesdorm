package table_panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class CurrentPermitTablePanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private Utilities util;
	private JLabel dateJL;
	private JLabel categoryJL;
	private JLabel nameJL;
	private JLabel reasonJL;
	private JLabel placeJL;
	
/*	private ArrayList<JButton> nameEntriesList; 
	private ArrayList<JTextField> categoryEntriesList;
	private ArrayList<JTextField> reasonEntriesList;
	private ArrayList<JTextField> placeEntriesList;*/
	
	private int xName;
	private int yName;
	private int xCategory;
	private int yCategory;
	private int xReason;
	private int yReason;
	private int xPlace;
	private int yPlace;
	public CurrentPermitTablePanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		
		xCategory = 20;
		yCategory= 80;
		xName = xCategory + 150;
		yName = yCategory;
		xReason = xName + 250;
		yReason = yCategory;
		xPlace = xReason + 300;
		yPlace = yCategory;
		
		addButtons();
		addLabels();
//		addTextFields();
/*		addIndivButtonAndTextField("Going home", "April Joy Padrigano","miss ya","Borongan");//macall la pag may data
		addIndivButtonAndTextField("Overnight", "mano","i Love you","with me");
		addIndivButtonAndTextField("Overnight", "oiuytrewlkjhgfds","fcvgbhjnu","with me");
		addIndivButtonAndTextField("Overnight", "kjhgfdytre","ghjou","with me");
		addIndivButtonAndTextField("Overnight", "Bhgvc","u","with me");
		addIndivButtonAndTextField("Overnight", "hgfsdf","asdfghj","with me");
		addIndivButtonAndTextField("Overnight", "hfgbvnbvcx","cvbnhnmu","kay crush");
		addIndivButtonAndTextField("Overnight", "kristine alpez","asa gad","puso mo");
		addIndivButtonAndTextField("Overnight", "hjfgdfg","cute","tacloban");
		addIndivButtonAndTextField("Overnight", "pfghfh","lonely","ha jupiter");*/
		
		
	}
	public void addButtons(){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 5, 50, 20, this);
//		nameEntriesList = new ArrayList<JButton>();
		
	}
	public void addLabels(){
		categoryJL = new JLabel();
		nameJL = new JLabel();
		reasonJL = new JLabel();
		placeJL = new JLabel();
		dateJL = new JLabel();
		util.labelProperties(dateJL, 700, 5, 100, 100, "Date:", this);
		util.labelProperties(categoryJL, 40, 20, 100, 100, "Category", this);
		util.labelProperties(nameJL, 250, 20, 100, 100, "Name", this);
		util.labelProperties(reasonJL, 480, 20, 100, 100, "Reason", this);
		util.labelProperties(placeJL, 750, 20, 100, 100, "Place", this);
		
	}
/*	public void addTextFields(){
		categoryEntriesList = new ArrayList<JTextField>();
		reasonEntriesList= new ArrayList<JTextField>();
		placeEntriesList= new ArrayList<JTextField>();
	}*/
	
/*	public void addIndivButtonAndTextField(String cat, String name,String reason, String place){
		categoryEntriesList.add(new JTextField());
		util.textFieldProperties(categoryEntriesList.get(categoryEntriesList.size()-1), xCategory, yCategory, 110, 30, this);
		nameEntriesList.add(new JButton(name));
		util.buttonProperties(nameEntriesList.get(nameEntriesList.size()-1), xName, yName, 200, 30, this);
		reasonEntriesList.add(new JTextField());
		util.textFieldProperties(reasonEntriesList.get(reasonEntriesList.size()-1), xReason, yReason, 250, 30, this);
		placeEntriesList.add(new JTextField());
		util.textFieldProperties(placeEntriesList.get(placeEntriesList.size()-1), xPlace, yPlace, 180, 30, this);
		
		//setting of text
		
		categoryEntriesList.get(categoryEntriesList.size()-1).setText(cat);
		reasonEntriesList.get(reasonEntriesList.size()-1).setText(reason);
		placeEntriesList.get(placeEntriesList.size()-1).setText(place);
		
		categoryEntriesList.get(categoryEntriesList.size()-1).setEditable(false);
		reasonEntriesList.get(reasonEntriesList.size()-1).setEditable(false);
		placeEntriesList.get(placeEntriesList.size()-1).setEditable(false);
		
		yCategory += 40;
		yName = yCategory;
		yReason = yCategory;
		yPlace = yCategory;
		
	}*/
/*	public void clearLists(){
		categoryEntriesList = new ArrayList<JTextField>();
		reasonEntriesList= new ArrayList<JTextField>();
		placeEntriesList= new ArrayList<JTextField>();
		nameEntriesList = new ArrayList<JButton>();
		xCategory = 20;
		yCategory= 80;
		xName = xCategory + 150;
		yName = yCategory;
		xReason = xName + 250;
		yReason = yCategory;
		xPlace = xReason + 300;
		yPlace = yCategory;
	}*/
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
	}
	public JButton getBack(){
		return back;
	}
}
