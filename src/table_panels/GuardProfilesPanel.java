package table_panels;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class GuardProfilesPanel extends JPanel {
	/**
	 * kelangan scrollable ini
	 * Edited on Oct 24 by AJP
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private Utilities util;
	private JLabel idNumberJL;
	private JLabel nameJL;
//	private ArrayList<JButton> viewList;
//	private ArrayList<JButton> editList;
//	private ArrayList<JButton> removeList;
	private ArrayList<JTextField> idNumberList;
	private ArrayList<JTextField> nameList;
	private JButton viewIndivJB;
	private JButton editIndivJB;
	private JButton removeIndivJB;
	private JButton addNewJB;
	private int xView;
	private int yView;
	private int xEdit;
	private int yEdit;
	private int xRemove;
	private int yRemove;
	private int xIDNum; 
	private int yIDNum; 
	private int xName;
	private int yName;
	private String nameOrSN;

	public GuardProfilesPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		declarations();
		addButtons();
		addLabels();
		addTextFields();
	
	//	addIndivButtonAndTextField("1234", "uytrew");
	//	addIndivButtonAndTextField("1234", "uytrew");
		
	}
	public void declarations(){
		xIDNum = 40;
		yIDNum = 80;
		xName = xIDNum +210;
		yName = yIDNum;
		xView = xName +280;
		yView = yIDNum;
		xEdit = xView + 160;
		yEdit = yView;
		xRemove = xEdit +160;
		yRemove = yEdit;
		
	
		
	}
	public void addButtons(){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 10, 50, 20, this);
		addNewJB = new JButton("Add New Guard");
//		viewList = new ArrayList<JButton>();
//		editList = new ArrayList<JButton>();
//		removeList = new ArrayList<JButton>();
		
		util.buttonProperties(addNewJB, 650, 10, 250, 20, this);
		viewIndivJB = new JButton("View a Profile");
		editIndivJB = new JButton("Edit a profile");
		removeIndivJB = new JButton("Remove a Profile");
		util.buttonProperties(viewIndivJB, 750, 100, 150, 50, this);
		util.buttonProperties(editIndivJB, 750, 150, 150, 50, this);
		util.buttonProperties(removeIndivJB,750, 200, 150, 50, this);
	}
	public void addLabels(){
		idNumberJL = new JLabel();
		nameJL = new JLabel();
		util.labelProperties(nameJL, 350, 40, 100, 50, "Name", this);
		util.labelProperties(idNumberJL, 45, 40,250, 50, "Identification Number", this);
	}
	public void addTextFields(){
		idNumberList = new ArrayList<JTextField>();
		nameList = new ArrayList<JTextField>();
	}
	public void addIndivButtonAndTextField(String idNum, String name/*ActionListener listen*/){
		idNumberList.add(new JTextField());
		util.textFieldProperties(idNumberList.get(idNumberList.size()-1), xIDNum, yIDNum, 180, 30, this);
		nameList.add(new JTextField());
		util.textFieldProperties(nameList.get(nameList.size()-1), xName, yName, 250, 30, this);
//		viewList.add(new JButton("View"));
//		util.buttonProperties(viewList.get(viewList.size()-1), xView, yView, 120, 30, this);
//		editList.add(new JButton("Edit"));
//		util.buttonProperties(editList.get(editList.size()-1), xEdit, yEdit, 120, 30, this);
//		removeList.add(new JButton("Remove"));
//		util.buttonProperties(removeList.get(removeList.size()-1), xRemove, yRemove, 120, 30, this);
		//setting of text & listeners
		idNumberList.get(idNumberList.size()-1).setText(idNum);
		nameList.get(nameList.size()-1).setText(name);
		
		//viewList.get(viewList.size()-1).addActionListener(listen);
		//editList.get(editList.size()-1).addActionListener(listen);
		//removeList.get(removeList.size()-1).addActionListener(listen);
		
		//set editable=false
		idNumberList.get(idNumberList.size()-1).setEditable(false);
		nameList.get(nameList.size()-1).setEditable(false);
		
		yIDNum += 40;
		yName = yIDNum;
		yView = yIDNum;
		yEdit = yView;
		yRemove = yEdit;
		
		
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		addNewJB.addActionListener(listen);
		addInsideListener(listen);
	}
	public void addInsideListener(ActionListener lis){
		viewIndivJB.addActionListener(lis);
		editIndivJB.addActionListener(lis);
		removeIndivJB.addActionListener(lis);
	}

	public JButton getBack(){return back;}
	public JButton getAddGuard(){ return addNewJB;}
	public JButton getViewIndiv(){ return viewIndivJB;}
	public JButton getEditIndiv(){ return editIndivJB;}
	public JButton getRemoveIndiv(){ return removeIndivJB;}
	
	
	
	public String getNameOrSn(){ return nameOrSN;}
	public void setNameOrSN(String sn){ nameOrSN = sn;}
	
}
