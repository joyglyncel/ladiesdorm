package vis.panels;
/*This is for the MENU BUTTONS.*/
import java.awt.*;

import javax.swing.*;

import jhunn_ate_visualManager.Utilities;

import java.awt.event.ActionListener;


public class Menu extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private ImageIcon background;
	private JButton guardJB;
	private JButton studentJB;
	private JButton dormMgJB;
	private JButton developersJB;
	@SuppressWarnings("unused")
	private ImageIcon titleImage;
	private Utilities util;
	public Menu(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,800);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		guardJB = new JButton(/*new ImageIcon(getClass().getResource("/start-d.png"))*/ "Guard");
		//guardJB.setRolloverIcon(new ImageIcon(getClass().getResource("/start-l.png")));
		studentJB = new JButton("Student");
		dormMgJB = new JButton("Dorm Manager");
		developersJB = new JButton("Developers");
		util.buttonProperties(guardJB, 410, 220, 200, 100, this);
		util.buttonProperties(studentJB, 410, 340,200, 100, this);
		util.buttonProperties(dormMgJB, 410, 460, 200, 100, this);
		util.buttonProperties(developersJB, 50, 100, 200, 100, this);
				
	}
	public void paintComponent(Graphics g){
		background = new ImageIcon(getClass().getResource("/cube.jpg"));
		background.paintIcon(this,g,0,0);
		//titleImage= new ImageIcon(getClass().getResource("/title.png")); 
		//titleImage.paintIcon(this, g,50, 100);
	}
	public void addListener(ActionListener listen){
		guardJB.addActionListener(listen);
		studentJB.addActionListener(listen);
		dormMgJB.addActionListener(listen);
		developersJB.addActionListener(listen);
	}
	public JButton getGuardJB(){ return guardJB; }
	public JButton getStudentJB(){ return studentJB; }
	public JButton getDormMGJB(){ return dormMgJB; }
	public JButton getDevelopersJB(){ return developersJB;}
}
