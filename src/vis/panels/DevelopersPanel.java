package vis.panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class DevelopersPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private Utilities util;
	public DevelopersPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		back = new JButton("Back");
		util.buttonProperties(back, 100, 400, 100, 100, this);
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
	}
	public JButton getBack(){
		return back;
	}
	
}
