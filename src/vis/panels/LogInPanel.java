package vis.panels;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class LogInPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JButton back;
	private JButton connectJB;
	private JLabel accountIDJL;	//should be changed to picture
	private JLabel passwordJL;	//should be changed to pic
	private JTextField accountIDTF;
	private JTextField passwordTF;
	private Utilities util;
	private StringBuffer categoryBuf;
	private JLabel categoryJL;
	private JButton forgotPasswordJB;
	private JButton noAccountJB;
	private TextFieldHandler tfHandler;
	public LogInPanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		categoryBuf = new StringBuffer();
		util = new Utilities();
		initializeObjects();
		tfHandler = new TextFieldHandler();
		addTFListener(tfHandler);
		
	}
	public void initializeObjects(){
		//buttons
		back = new JButton("Back");
		connectJB = new JButton("Connect!");
		forgotPasswordJB = new JButton("Forgot password?");
		noAccountJB = new JButton("No account yet?");
		util.buttonProperties(back, 40, 50, 100, 100, this);
		util.buttonProperties(connectJB, 380, 500, 100, 100, this);
		util.buttonProperties(forgotPasswordJB, 450, 380, 200, 50, this);
		util.buttonProperties(noAccountJB, 450, 450, 200, 50, this);
		//labels
		accountIDJL = new JLabel();
		passwordJL = new JLabel("Password: ");
		util.labelProperties(accountIDJL, 300, 220, 200, 100, "Username:", this);
		util.labelProperties(passwordJL, 300, 300, 150, 100, "Password: ", this);
		//textfields
		accountIDTF = new JTextField(15);
		passwordTF = new JTextField(15);
		util.textFieldProperties(accountIDTF, 400, 250, 200, 50, this);
		util.textFieldProperties(passwordTF, 400, 320, 200, 50, this);
		categoryJL = new JLabel();
	}
	public void setCategory(String str){
		categoryBuf = new StringBuffer();
		categoryBuf.append(str);
		util.labelProperties(categoryJL, 200, 50, 600, 100, categoryBuf.toString(), this);
	}
	public String getCategory(){
		return categoryBuf.toString();
	}
	public void setTFToNull(){
		accountIDTF.setText(null);
		passwordTF.setText(null);
	}
	public String getAccountIDStr(){
		return accountIDTF.getText();
	}
	public String getPasswordStr(){
		return passwordTF.getText();
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		connectJB.addActionListener(listen);
		forgotPasswordJB.addActionListener(listen);
		noAccountJB.addActionListener(listen);
	}
	public void addTFListener(ActionListener listen){
		accountIDTF.addActionListener(listen);
		passwordTF.addActionListener(listen);
		
	}
	public JButton getBack(){ return back; }
	public JButton getConnectJB(){ return connectJB;}
	public JButton getForgotPasswordJB(){ return forgotPasswordJB;}
	public JButton getNoAccountJB(){ return noAccountJB;}
	private class TextFieldHandler implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			if(e.getSource() == accountIDTF){
				errorTrapTF(accountIDTF, 1);
			}else if(e.getSource() == passwordTF){
				errorTrapTF(passwordTF, 3);
			}
		}
		
	}
	public void errorTrapTF(JTextField field, int type){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(this, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
	
}
