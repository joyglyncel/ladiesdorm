package vis.panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class StudentPanel extends JPanel{
	/**
	 * after loginPanel
	 */
	private static final long serialVersionUID = 1L;

	private Utilities util;
	private String category;
	private JButton roomJB;
	private JButton submitPermitJB;
	private JButton editProfileJB;
	private JButton logOutJB;
	public StudentPanel(){
		category = new String("Student");
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		addButtons();
	}
	public void addButtons(){
		roomJB = new JButton("Room");
		submitPermitJB = new JButton("Submit Permit");
		editProfileJB = new JButton("Edit Profile");
		logOutJB = new JButton("Log Out");
		util.buttonProperties(roomJB, 150, 150, 100, 100, this);
		util.buttonProperties(submitPermitJB, 250, 350, 100, 100, this);
		util.buttonProperties(editProfileJB, 450, 150, 100, 100, this);
		util.buttonProperties(logOutJB, 450, 20, 100, 50, this);
	}
	public void addListener(ActionListener listen){
		roomJB.addActionListener(listen);
		submitPermitJB.addActionListener(listen);
		editProfileJB.addActionListener(listen);
		logOutJB.addActionListener(listen);
	}
	public JButton getRoomJB(){
		return roomJB;
	}
	public JButton getSubmitPermitJB(){
		return submitPermitJB;
	}
	public JButton getEditProfileJB(){
		return editProfileJB;
	}
	public String getCategory(){
		return category;
	}
	public JButton getLogOutJB(){
		return logOutJB;
	}
}
