package vis.panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class DormMgPanel extends JPanel {
	/**
	 * after logIn Panel
	 */
	private static final long serialVersionUID = 1L;
	private Utilities util;
	private String category;
	private JButton roomsJB;	//maadd hin button pag human na an usa nga functionality
	private JButton permitsToday;
	private JButton studentProfilesJB;
	private JButton guardProfilesJB;
	private JButton dormMgrProfileJB;
	private JButton logOutJB;
	public DormMgPanel(){
		category = new String("Dorm Manager");
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		addButtons();
		
	//	addLabels();
	}
	public void addButtons(){

		roomsJB = new JButton("Rooms");
		util.buttonProperties(roomsJB, 200, 200, 100, 100, this);
		dormMgrProfileJB = new JButton("Dorm Manager");
		util.buttonProperties(dormMgrProfileJB, 400, 200, 150, 100, this);
		permitsToday = new JButton("View Permits Today");
		util.buttonProperties(permitsToday, 350, 450, 150, 100, this);
		studentProfilesJB = new JButton("Student Profiles");
		util.buttonProperties(studentProfilesJB, 550, 350, 150, 100, this);
		guardProfilesJB = new JButton("Guard Profiles");
		util.buttonProperties(guardProfilesJB, 550, 250, 150, 100, this);
		logOutJB = new JButton("Log Out");
		util.buttonProperties(logOutJB, 500, 20, 50, 20, this);
	}
	//public void addLabels(){
		
//	}
	public void addListener(ActionListener listen){
		roomsJB.addActionListener(listen);
		dormMgrProfileJB.addActionListener(listen);
		permitsToday.addActionListener(listen);
		studentProfilesJB.addActionListener(listen);
		guardProfilesJB.addActionListener(listen);
		logOutJB.addActionListener(listen);
	}
	
	
	public JButton getRoomsJB(){ return roomsJB;}
	public String getCategory(){ return category;}
	public JButton getDormMgrProfileJB(){ return dormMgrProfileJB;}
	public JButton getPermitsTodayJB(){ return permitsToday;}
	public JButton getStudentProfilesJB(){ return studentProfilesJB;}
	public JButton getGuardProfilesJB(){ return guardProfilesJB;}
	public JButton getLogOutJB(){ return logOutJB;}
	
}
