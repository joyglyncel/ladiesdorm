package jhunn_ate_visualManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JOptionPane;

import edit_profiles_and_permit.EditIndivDormMgrPanel;
import edit_profiles_and_permit.EditIndivGuardPanel;
import edit_profiles_and_permit.EditIndivPermitPanel;
import edit_profiles_and_permit.EditIndivStudentPanel;
import jhunn_entities.ActorDormManager;
import jhunn_entities.ActorGuard;
import jhunn_entities.ActorStudent;
import rooms_and_permits_panels.RoomsInsidePanel;
import rooms_and_permits_panels.RoomsSquarePanel;
import sign_up_panels.SignUpDormManagerPanel;
import sign_up_panels.SignUpGuardPanel;
import sign_up_panels.SignUpStudentPanel;
import table_panels.CurrentPermitTablePanel;
import table_panels.GuardProfilesPanel;
import table_panels.StudentProfilesPanel;
import view_profiles.ViewIndivDormMgrPanel;
import view_profiles.ViewIndivGuardPanel;
import view_profiles.ViewIndivStudentPanel;
import vis.panels.DevelopersPanel;
import vis.panels.DormMgPanel;
import vis.panels.GuardPanel;
import vis.panels.LogInPanel;
import vis.panels.Menu;
import vis.panels.StudentPanel;

public class Control {
	private View view;
	private Menu menu;
	private DormMgPanel dormMgPanel;
	private GuardPanel guardPanel;
	private StudentPanel studentPanel;
	private LogInPanel logInPanel;
	private SignUpDormManagerPanel signUpDormManagerPanel;
	private SignUpGuardPanel signUpGuardPanel;
	private SignUpStudentPanel signUpStudentPanel;
	private DevelopersPanel developersPanel;
	private RoomsSquarePanel roomsSquarePanel;
	private RoomsInsidePanel roomsInsidePanel;
	private EditIndivGuardPanel editIndivGuardPanel;
	private EditIndivStudentPanel editIndivStudentPanel;
	private EditIndivDormMgrPanel editIndivDormMgrPanel;
	private EditIndivPermitPanel editIndivPermitPanel;
	private CurrentPermitTablePanel currentPermitTablePanel;
	private StudentProfilesPanel studentProfilesPanel;
	private GuardProfilesPanel guardProfilesPanel;
	private ViewIndivDormMgrPanel viewIndivDormMgrPanel;
	private ViewIndivGuardPanel viewIndivGuardPanel;
	private ViewIndivStudentPanel viewIndivStudentPanel;
	private ActorDormManager dormManager;
	private ActorGuard guard;
	private ActorStudent student;
	
	private boolean dormMgrBol;
	private boolean guardBol;
	private boolean studentBol;
	private boolean dormTurn;
	private boolean guardTurn;
	private boolean guardPermitTurn;
	private boolean dormMgrPermitTurn;
	private boolean profGuardTurn;
	private boolean profDormTurn;
	private boolean studentProfTurn;
	private boolean dormProfTurn;
	private boolean viewClicked;
	private boolean editClicked;
	private boolean removeClicked;
	private boolean guardProfIn;
	private boolean guardProfEdit;
	private boolean studentProfView;
	private boolean studentProfEdit;
	private boolean studentProfRemove;
	private boolean dormAdd;
	
	
	private String nameOrSN;
	public Control(View view){	
		this.view = view;
		studentProfTurn = false;
		dormProfTurn = false;
		dormMgrBol = false;
		guardBol = false;
		studentBol = false;
		dormTurn = false;
		guardTurn = false;
		guardPermitTurn = false;
		dormMgrPermitTurn = false;
		profGuardTurn = false;
		profDormTurn = false;
		viewClicked = false;
		editClicked = false;
		removeClicked = false;
		guardProfIn = false;
		guardProfEdit = false;
		studentProfView = false;
		studentProfEdit = false;
		studentProfRemove = false;
		dormAdd = false;
		dormManager = new ActorDormManager();
		guard = new ActorGuard();
		student = new ActorStudent();//tas loading from database it single Info
	}
	//firstcall this
	public void setAllPanels(){
		//unite panels here with view panels
		setPanelsToView();
		//initialize button listener for all panels
		initPanelsButtons();
	}
	public void setPanelsToView(){
		this.menu = view.getMenu();
		this.dormMgPanel = view.getDormMgPanel();
		this.guardPanel = view.getGuardPanel();
		this.studentPanel = view.getStudentPanel();
		this.logInPanel = view.getLogInPanel();
		this.signUpDormManagerPanel = view.getSignUpPanel();
		this.signUpGuardPanel = view.getSignUpGuardPanel();
		this.signUpStudentPanel = view.getSignUpStudentPanel();
		this.developersPanel = view.getDevelopersPanel();
		this.roomsSquarePanel = view.getRoomsSquarePanel();
		this.roomsInsidePanel = view.getRoomsInsidePanel();
		this.editIndivGuardPanel = view.getEditIndivGuardPanel();
		this.editIndivStudentPanel = view.getEditIndivStudentPanel();
		this.editIndivDormMgrPanel = view.getEditIndivDormMgrPanel();
		this.editIndivPermitPanel = view.getEditIndivPermitPanel();
		this.currentPermitTablePanel = view.getCurrentPermitTablePanel();
		this.studentProfilesPanel = view.getStudentProfilesPanel();
		this.guardProfilesPanel = view.getGuardProfilesPanel();
		this.viewIndivDormMgrPanel= view.getViewIndivDormMgrPanel();
		this.viewIndivGuardPanel = view.getViewIndivGuardPanel();
		this.viewIndivStudentPanel = view.getViewIndivStudentPanel();
	}
	public void initPanelsButtons(){
		this.menu.addListener(new ButtonListener());
		this.dormMgPanel.addListener(new ButtonListener());
		this.guardPanel.addListener(new ButtonListener());
		this.studentPanel.addListener(new ButtonListener());
		this.logInPanel.addListener(new ButtonListener());
		this.signUpDormManagerPanel.addListener(new ButtonListener());
		this.signUpGuardPanel.addListener(new ButtonListener());
		this.signUpStudentPanel.addListener(new ButtonListener());
		this.developersPanel.addListener(new ButtonListener());
		this.roomsSquarePanel.addListener(new ButtonListener());
		this.roomsInsidePanel.addListener(new ButtonListener());
		this.editIndivGuardPanel.addListener(new ButtonListener());
		this.editIndivStudentPanel.addListener(new ButtonListener());
		this.editIndivDormMgrPanel.addListener(new ButtonListener());
		this.editIndivPermitPanel.addListener(new ButtonListener());
		this.currentPermitTablePanel.addListener(new ButtonListener());
		this.studentProfilesPanel.addListener(new ButtonListener());
		this.guardProfilesPanel.addListener(new ButtonListener());
		this.viewIndivDormMgrPanel.addListener(new ButtonListener());
		this.viewIndivGuardPanel.setButtonListener(new ButtonListener());
		this.viewIndivStudentPanel.setButtonHandler(new ButtonListener());
	}
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent event){
			try{
				if(event.getSource() == menu.getGuardJB()){
					guardBol = true;
					view.getLogInPanel().setCategory("Guard");
					view.Switch("LogInPanel");		
				}else if(event.getSource() == menu.getDevelopersJB()){
					view.Switch("DevelopersPanel");
				}else if(event.getSource() == developersPanel.getBack()){
					view.Switch("Menu");
				}else if(event.getSource() == menu.getStudentJB()){
					studentBol = true;
					view.getLogInPanel().setCategory("Student");
					view.Switch("LogInPanel");
				}else if(event.getSource() == menu.getDormMGJB()){
					dormMgrBol = true;
					view.getLogInPanel().setCategory("Dorm Manager");
					view.Switch("LogInPanel");
				}else if(event.getSource() == logInPanel.getConnectJB()){
					if(dormMgrBol){
						dormMgrBol = false;
						view.Switch("DormMgPanel");
					}else if(guardBol){
						guardBol = false;
						view.Switch("GuardPanel");
					}else if(studentBol){
						studentBol = false;
						view.Switch("StudentPanel");
					}
				}else if(event.getSource() == logInPanel.getForgotPasswordJB()){
					JOptionPane.showMessageDialog(logInPanel, "Contact Dorm Manager to get a new one.", "Password Renew", 2);
				}else if(event.getSource() == logInPanel.getNoAccountJB()){
					JOptionPane.showMessageDialog(logInPanel, "Approach Dorm Manager to create one.", "Get New Account", 2);
				}else if(event.getSource() == dormMgPanel.getPermitsTodayJB()){
					dormMgrPermitTurn = true;
					view.Switch("CurrentPermitTablePanel");
				}else if(event.getSource() == dormMgPanel.getRoomsJB()){
					dormTurn = true;
					view.Switch("RoomsSquarePanel");
				}else if(event.getSource() == dormMgPanel.getDormMgrProfileJB()){
					view.Switch("EditIndivDormMgrPanel");
				}else if(event.getSource() == dormMgPanel.getStudentProfilesJB()){
					view.Switch("StudentProfilesPanel");
				}else if(event.getSource() == studentProfilesPanel.getBack()){
					view.Switch("DormMgPanel");
				}else if(event.getSource() == studentProfilesPanel.getAddStudentJB()){
					dormProfTurn = true;
					dormAdd = true;
					view.Switch("EditIndivStudentPanel");
				}else if(event.getSource() == studentProfilesPanel.getViewIndivJB()){
					studentProfView = true;
					
					nameOrSN= JOptionPane.showInputDialog("Enter name or student number:");
					view.Switch("ViewIndivStudentPanel");
				}else if(event.getSource() == studentProfilesPanel.getEditIndivJB()){
					studentProfEdit = true;
					nameOrSN= JOptionPane.showInputDialog("Enter name or student number:");
					view.Switch("EditIndivStudentPanel");
				}else if(event.getSource() == studentProfilesPanel.getRemoveINdivJB()){
					studentProfRemove = true;
					JOptionPane.showMessageDialog(null, "Warning! Student will be removed.");
				}else if(event.getSource() == viewIndivStudentPanel.getBack()){
					if(studentProfView){
						studentProfView = false;
						view.Switch("StudentProfilesPanel");
					}else{
						
					}
				}else if(event.getSource() == dormMgPanel.getLogOutJB()){
					view.Switch("Menu");
				}else if(event.getSource() == dormMgPanel.getGuardProfilesJB()){
			
					view.Switch("GuardProfilesPanel");
				}else if(event.getSource() == guardProfilesPanel.getBack()){
					view.Switch("DormMgPanel");
				}else if(event.getSource() == guardProfilesPanel.getAddGuard()){
					profDormTurn = true;
					view.Switch("EditIndivGuardPanel");//tas database
				}else if(event.getSource() == guardProfilesPanel.getViewIndiv()){
					guardProfIn = true;
					
					nameOrSN= JOptionPane.showInputDialog("Enter name or student number:");
					
					view.Switch("ViewIndivGuardPanel");
				}else if(event.getSource() == viewIndivGuardPanel.getBack()){
					if(guardProfIn){
						guardProfIn = false;
						view.Switch("GuardProfilesPanel");
					}
				}else if(event.getSource() == guardProfilesPanel.getEditIndiv()){
					
					guardProfEdit = true;
					nameOrSN= JOptionPane.showInputDialog("Enter name or student number:");
					view.Switch("EditIndivGuardPanel");
					
				}else if(event.getSource() == guardProfilesPanel.getRemoveIndiv()){
					JOptionPane.showMessageDialog(null, "Warning! Student will be removed.");
				
				}else if(event.getSource() == editIndivDormMgrPanel.getBack()){
					int ans = JOptionPane.showConfirmDialog(editIndivGuardPanel, "Changes will be lost if pursued.Continue?", "Are you sure?", 0);
					if(ans == 0){//if yes
						view.Switch("DormMgPanel");
					}
					
				}else if(event.getSource() == editIndivDormMgrPanel.getSave()){
					JOptionPane.showMessageDialog(editIndivGuardPanel, "Successfully modified!", "Profile Updated", 2);
					view.Switch("DormMgPanel");
				}else if(event.getSource() == roomsSquarePanel.getBack()){
					if(dormTurn){
						dormTurn = false;
						view.Switch("DormMgPanel");
					}else if(guardTurn){
						guardTurn = false;
						view.Switch("GuardPanel");
					}
				}else if(event.getSource() == roomsSquarePanel.getRoom0()){
					view.getRoomsInsidePanel().setRoomNo(1);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom1()){
					view.getRoomsInsidePanel().setRoomNo(2);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom2()){
					view.getRoomsInsidePanel().setRoomNo(3);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom3()){
					view.getRoomsInsidePanel().setRoomNo(4);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom4()){
					view.getRoomsInsidePanel().setRoomNo(5);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom5()){
					view.getRoomsInsidePanel().setRoomNo(6);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom6()){
					view.getRoomsInsidePanel().setRoomNo(7);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom7()){
					view.getRoomsInsidePanel().setRoomNo(8);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom8()){
					view.getRoomsInsidePanel().setRoomNo(9);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom9()){
					view.getRoomsInsidePanel().setRoomNo(10);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom10()){
					view.getRoomsInsidePanel().setRoomNo(11);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsSquarePanel.getRoom11()){
					view.getRoomsInsidePanel().setRoomNo(12);
					view.Switch("RoomsInsidePanel");
				}else if(event.getSource() == roomsInsidePanel.getBack()){//an pag view han indiv profile na panel
					view.Switch("RoomsSquarePanel");
				}else if(event.getSource() == studentPanel.getRoomJB()){
					
				}else if(event.getSource() == studentPanel.getLogOutJB()){
					view.Switch("Menu");
				}else if(event.getSource() == studentPanel.getSubmitPermitJB()){
					view.Switch("EditIndivPermitPanel");
				}else if(event.getSource() == editIndivPermitPanel.getBack()){
					int i = JOptionPane.showConfirmDialog(editIndivPermitPanel, "Changes will be unsaved. Continue?", "Unsaved Changes", 0);
					if(i == 0){//if yes else do nothing
						view.Switch("StudentPanel");
					}
				}else if(event.getSource() == editIndivPermitPanel.getSave()){
					JOptionPane.showMessageDialog(editIndivStudentPanel, "Successfully modified!", "Permit submitted!", 2);
					view.Switch("StudentPanel");
				}else if(event.getSource() == studentPanel.getEditProfileJB()){
					studentProfTurn = true;
					view.Switch("EditIndivStudentPanel");
				}else if(event.getSource() == editIndivStudentPanel.getBack()){
					int i = JOptionPane.showConfirmDialog(editIndivStudentPanel, "Changes will be unsaved. Continue?", "Unsaved Changes", 0);
					if(i == 0){//if yes else do nothing
						if(dormProfTurn){//add student button
							dormProfTurn = false;
							view.Switch("StudentProfilesPanel");
						}else if(studentProfTurn){// view button
							studentProfTurn = false;
							view.Switch("StudentPanel");
							
						}else if(studentProfEdit){//student profiles panel - edit button
							studentProfEdit = false;
							view.Switch("StudentProfilesPanel");
						}
						
					}
				}else if(event.getSource() == editIndivStudentPanel.getSave()){
					JOptionPane.showMessageDialog(editIndivStudentPanel, "Successfully modified!", "Profile updated !", 2);
					if(studentProfEdit){//student table profiles
						studentProfEdit = false;
						view.Switch("StudentProfilesPanel");
					}else if(studentProfTurn){
						studentProfTurn = false;
						view.Switch("StudentPanel");
					}else if(dormAdd){// add student
						dormAdd = false;
						view.Switch("StudentProfilesPanel");
						
					}
				}else if(event.getSource() == guardPanel.getViewPermitJB()){
					guardPermitTurn = true;
					view.Switch("CurrentPermitTablePanel");
				}else if(event.getSource() == currentPermitTablePanel.getBack()){
					if(guardPermitTurn){
						guardPermitTurn = false;
						view.Switch("GuardPanel");
					}else if(dormMgrPermitTurn){
						view.Switch("DormMgPanel");
					}
					
				}else if(event.getSource() == guardPanel.getRoomJB()){
					guardTurn = true;
					view.Switch("RoomsSquarePanel");
				}else if(event.getSource() == guardPanel.getLogOutJB()){
					view.Switch("Menu");
				}else if(event.getSource() == guardPanel.getEditProfileJB()){
					profGuardTurn = true;
					view.Switch("EditIndivGuardPanel");
				}else if(event.getSource() == editIndivGuardPanel.getBack()){
					int ans = JOptionPane.showConfirmDialog(editIndivGuardPanel, "Changes will be lost if pursued.Continue?", "Are you sure?", 0);
					if(ans == 0){//if yes
						if(profDormTurn){
							profDormTurn = false;
							view.Switch("GuardProfilesPanel");
						}else if(profGuardTurn){
							profGuardTurn = false;
							view.Switch("GuardPanel");
						}else if(guardProfEdit){
							guardProfEdit = false;
							view.Switch("GuardProfilesPanel");
						}
						
					}
					
				}else if(event.getSource() == editIndivGuardPanel.getSaveJB()){
					//database update
					JOptionPane.showMessageDialog(editIndivGuardPanel, "Successfully modified!", "Profile Updated", 2);
			
					if(guardProfEdit){
						guardProfEdit = false;
						view.Switch("GuardProfilesPanel");
					}else if(profGuardTurn){
						profGuardTurn = false;
						view.Switch("GuardProfilesPanel");
					}else if(profDormTurn){
						profDormTurn = false;
						view.Switch("GuardProfilesPanel");
					}
				}else if(event.getSource() == logInPanel.getBack()){
					view.Switch("Menu");
				}
			}catch(Exception e){
				e.printStackTrace();
				JOptionPane.showMessageDialog(menu, "Sorry something went wrong.");
			}
		}	
	}
}
