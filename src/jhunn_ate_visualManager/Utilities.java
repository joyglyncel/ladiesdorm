package jhunn_ate_visualManager;

import java.awt.Color;
import java.awt.GridBagConstraints;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class Utilities {
	public void textAreaProperties( JTextArea area, int x, int y, int width, int height, JPanel panel){
		area.setBounds(x,y, width, height);
		area.setBackground(Color.DARK_GRAY);
		area.setForeground(Color.white);
		area.setText(null);	
		area.setWrapStyleWord(true);
		JScrollPane scrollPane = new JScrollPane(area);
		scrollPane.setBounds(20, 120, 340, 280);
        GridBagConstraints c = new GridBagConstraints();
        c.gridwidth = GridBagConstraints.REMAINDER;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.fill = GridBagConstraints.BOTH;
        c.weightx = 1.0;
        c.weighty = 50.0;
	    panel.add(scrollPane, c);
  }
	public void textFieldProperties(JTextField textField, int x, int y, int width, int height, JPanel panel){
		textField.setBounds(x, y, width, height);
		textField.setBackground(Color.WHITE);
		textField.setForeground(Color.BLACK);
		textField.setText(null);
		panel.add(textField);
	}
	public void buttonProperties(JButton button, int x, int y, int width, int height, JPanel panel){
		button.setBorder(null);
		button.setOpaque(false);
		button.setContentAreaFilled(true);
		button.setBorderPainted(false);
		button.setBounds(x, y, width, height);
		button.setFocusable(false);
		panel.add(button);
	}
	public void labelProperties(JLabel label, int x, int y, int width, int height, String text, JPanel panel){
		label.setForeground(Color.BLACK);
		label.setBounds(x, y, width, height);	
		if(label.getText() != null){
			label.setText(null);
			label.setText(text);
		}else{
			label.setText(text);
		}
		panel.add(label);
	}
	public void errorTrapTF(JTextField field, int type,JPanel panel){
		boolean done = false;
		if(type == 1){ //for numbers
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if( (array[i] >= 'a') && (array[i] <= 'z') || ( (array[i] >= 'A') && (array[i] <= 'Z'))){
					JOptionPane.showMessageDialog(panel, "You can't write or mix letters in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else if(type == 2){ //for letters
			String expression = field.getText();
			char[] array = new char[100];
			array = expression.toCharArray();
			for(int i =0; i< array.length ;i++){
				if(  ( (array[i] >= '0') && (array[i] <= '9'))){
					JOptionPane.showMessageDialog(panel, "You can't write or mix numbers in input field.");
					done = false;
					break;
				}
				else{
					done = true;
				}
			}
			if(!done){
				expression = null;
				field.setText(null);
				field.setEditable(true);		
			}else{
				field.setEditable(false);
				done = false;
				field.requestFocus();
			}
		}else{//for letters and numbers
			done = false;
			field.setEditable(false);
			field.requestFocus();
		}
	}
	
}
