package jhunn_ate_visualManager;
import java.awt.*;

import javax.swing.*;

import edit_profiles_and_permit.EditIndivDormMgrPanel;
import edit_profiles_and_permit.EditIndivGuardPanel;
import edit_profiles_and_permit.EditIndivPermitPanel;
import edit_profiles_and_permit.EditIndivStudentPanel;
import rooms_and_permits_panels.RoomsInsidePanel;
import rooms_and_permits_panels.RoomsSquarePanel;
import sign_up_panels.SignUpDormManagerPanel;
import sign_up_panels.SignUpGuardPanel;
import sign_up_panels.SignUpStudentPanel;
import table_panels.CurrentPermitTablePanel;
import table_panels.GuardProfilesPanel;
import table_panels.StudentProfilesPanel;
import view_profiles.ViewIndivDormMgrPanel;
import view_profiles.ViewIndivGuardPanel;
import view_profiles.ViewIndivStudentPanel;
import vis.panels.DevelopersPanel;
import vis.panels.DormMgPanel;
import vis.panels.GuardPanel;
import vis.panels.LogInPanel;
import vis.panels.Menu;

import vis.panels.StudentPanel;

public class View extends JFrame {
	
	private static final long serialVersionUID = 1L;
	private Menu menu;
	private DormMgPanel dormMgPanel;
	private GuardPanel guardPanel;
	private StudentPanel studentPanel;
	private LogInPanel logInPanel;
	private SignUpDormManagerPanel signUpDormManagerPanel;
	private SignUpGuardPanel signUpGuardPanel;
	private SignUpStudentPanel signUpStudentPanel;
	private DevelopersPanel developersPanel;
	private RoomsSquarePanel roomsSquarePanel;
	private RoomsInsidePanel roomsInsidePanel;
	private EditIndivGuardPanel editIndivGuardPanel;
	private EditIndivStudentPanel editIndivStudentPanel;
	private EditIndivDormMgrPanel editIndivDormMgrPanel;
	private EditIndivPermitPanel editIndivPermitPanel;
	private CurrentPermitTablePanel currentPermitTablePanel;
	private StudentProfilesPanel studentProfilesPanel;
	private GuardProfilesPanel guardProfilesPanel;
	private ViewIndivDormMgrPanel viewIndivDormMgrPanel;
	private ViewIndivGuardPanel viewIndivGuardPanel;
	private ViewIndivStudentPanel viewIndivStudentPanel;
	private CardLayout layout;
	private JPanel panel;
	
	public View(){
		
		super("Ladies Dorm Logging Application");
		setLayout(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(1000, 700);
		setLocationRelativeTo(null);
		setResizable(false);
		initializePanels();
		
		panelProperties(menu);
		layout = new CardLayout();
		panel = new JPanel();
		panel.setBounds(0,0,1000,700);
		panel.setLayout(layout);
		addToMainPanel();
		add(panel);
		setVisible(true);
	}
	public void initializePanels(){
		menu = new Menu();
		dormMgPanel = new DormMgPanel();
		guardPanel = new GuardPanel();
		studentPanel = new StudentPanel();
		logInPanel = new LogInPanel();
		signUpDormManagerPanel = new SignUpDormManagerPanel();
		signUpGuardPanel = new SignUpGuardPanel();
		signUpStudentPanel = new SignUpStudentPanel();
		developersPanel = new DevelopersPanel();
		roomsSquarePanel = new RoomsSquarePanel();
		roomsInsidePanel = new RoomsInsidePanel();
		editIndivGuardPanel = new EditIndivGuardPanel();
		editIndivStudentPanel = new EditIndivStudentPanel();
		editIndivDormMgrPanel = new EditIndivDormMgrPanel();
		editIndivPermitPanel = new EditIndivPermitPanel();
		currentPermitTablePanel = new CurrentPermitTablePanel();
		studentProfilesPanel = new StudentProfilesPanel();
		guardProfilesPanel = new GuardProfilesPanel();
		viewIndivDormMgrPanel = new ViewIndivDormMgrPanel();
		viewIndivGuardPanel = new ViewIndivGuardPanel();
		viewIndivStudentPanel = new ViewIndivStudentPanel();
		
	}
	public void addToMainPanel(){
		panel.add(menu, "Menu");
		panel.add(guardPanel, "GuardPanel");
		panel.add(studentPanel, "StudentPanel");
		panel.add(dormMgPanel, "DormMgPanel");
		panel.add(logInPanel, "LogInPanel");
		panel.add(signUpDormManagerPanel, "SignUpDormManagerPanel");
		panel.add(signUpGuardPanel, "SignUpGuardPanel");
		panel.add(signUpStudentPanel, "SignUpStudentPanel");
		panel.add(developersPanel, "DevelopersPanel");
		panel.add(roomsSquarePanel, "RoomsSquarePanel");
		panel.add(roomsInsidePanel, "RoomsInsidePanel");
		panel.add(editIndivGuardPanel, "EditIndivGuardPanel");
		panel.add(editIndivStudentPanel, "EditIndivStudentPanel");
		panel.add(editIndivDormMgrPanel, "EditIndivDormMgrPanel");
		panel.add(editIndivPermitPanel, "EditIndivPermitPanel");
		panel.add(currentPermitTablePanel, "CurrentPermitTablePanel");
		panel.add(studentProfilesPanel, "StudentProfilesPanel");
		panel.add(guardProfilesPanel, "GuardProfilesPanel");
		panel.add(viewIndivDormMgrPanel,"ViewIndivDormMgrPanel");
		panel.add(viewIndivGuardPanel, "ViewIndivGuardPanel");
		panel.add(viewIndivStudentPanel, "ViewIndivStudentPanel");
		
	}
	public void panelProperties(JPanel panel){
		panel.setBounds(0,0,400,500);
		panel.setFocusable(false);
	}
	public void Switch(String str){
		layout.show(panel, str);
	}
	public ViewIndivDormMgrPanel  getViewIndivDormMgrPanel(){ return viewIndivDormMgrPanel;}
	public ViewIndivGuardPanel getViewIndivGuardPanel(){return viewIndivGuardPanel;}
	public ViewIndivStudentPanel getViewIndivStudentPanel(){return viewIndivStudentPanel;}
	public Menu getMenu(){	return menu; }
	public GuardPanel getGuardPanel(){ return guardPanel; }
	public StudentPanel getStudentPanel(){ return studentPanel; }
	public DormMgPanel getDormMgPanel(){ return dormMgPanel; }
	public LogInPanel getLogInPanel(){ return logInPanel;}
	public SignUpDormManagerPanel getSignUpPanel(){ return signUpDormManagerPanel; }
	public SignUpGuardPanel getSignUpGuardPanel(){ return signUpGuardPanel; }
	public SignUpStudentPanel getSignUpStudentPanel(){ return signUpStudentPanel; }
	public DevelopersPanel getDevelopersPanel(){ return developersPanel; }
	public RoomsSquarePanel getRoomsSquarePanel(){ return roomsSquarePanel;}
	public RoomsInsidePanel getRoomsInsidePanel(){return roomsInsidePanel;}
	public EditIndivGuardPanel getEditIndivGuardPanel(){ return editIndivGuardPanel;}
	public EditIndivStudentPanel getEditIndivStudentPanel(){return editIndivStudentPanel;}
	public EditIndivDormMgrPanel getEditIndivDormMgrPanel(){ return editIndivDormMgrPanel;}
	public EditIndivPermitPanel getEditIndivPermitPanel(){ return editIndivPermitPanel;}
	public CurrentPermitTablePanel getCurrentPermitTablePanel(){ return currentPermitTablePanel;}
	public StudentProfilesPanel getStudentProfilesPanel(){ return studentProfilesPanel;}
	public GuardProfilesPanel getGuardProfilesPanel(){ return guardProfilesPanel;}
}

