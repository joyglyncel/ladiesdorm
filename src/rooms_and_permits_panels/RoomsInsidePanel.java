package rooms_and_permits_panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class RoomsInsidePanel extends JPanel{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int roomNo;
	private JButton back;
	private Utilities util;
	private JLabel roomNoJL;
	private JLabel dateJL;
	private JLabel occupantsNamesJL;
	private JLabel inJL;
	private JLabel outJL;
	private JLabel[] bedNoJL;
	private JButton[] occupantsNamesJB;
	private int xJL;
	private int yJL;
	private int xJB;
	private int yJB;
	public RoomsInsidePanel(){
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		xJL = 100;
		yJL= 50;
		xJB= xJL + 40;
		yJB = yJL;
		addButtons();
		addLabels();
	}
	public void addButtons(){
		back = new JButton("Back");
		util.buttonProperties(back, 50, 20, 70, 50, this);
		occupantsNamesJB = new JButton[8];
		for(int i = 0; i < 8 ;i++){
			occupantsNamesJB[i] = new JButton();
			util.buttonProperties(occupantsNamesJB[i], xJB, yJB+=65, 200, 50, this);
		}
	}
	public void addLabels(){
		roomNoJL = new JLabel();
		dateJL = new JLabel();
		occupantsNamesJL = new JLabel();
		inJL = new JLabel();
		outJL = new JLabel();
		util.labelProperties(roomNoJL, 450, 20, 100, 50, "Room No.", this);
		util.labelProperties(dateJL, 550, 80, 40, 50, "Date:", this);
		util.labelProperties(occupantsNamesJL, 50, 70, 200, 50, "Occupants' Names", this);
		util.labelProperties(inJL, 380, 70, 100, 50, "IN", this);
		util.labelProperties(outJL, 420, 70, 100, 50, "OUT", this);
		bedNoJL = new JLabel[8];
		for(int i =0; i < 8; i++){
			bedNoJL[i] = new JLabel();
			util.labelProperties(bedNoJL[i], xJL, yJL+=40, 30, 20, i+1 + "", this);
		}
		
		
		
		
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		for(int i = 0 ;i< 8; i++){
			occupantsNamesJB[i].addActionListener(listen);
		}
	}
	public void setRoomNo(int room){
		roomNo = room;
		util.labelProperties(roomNoJL, 450, 50, 100, 50, "Room No." + roomNo, this);
	}
	
	public int getRoomNo(){
		return roomNo;
	}
	public JButton getBack(){ return back;}
	public JButton getOccupant0(){ return occupantsNamesJB[0];}
	public JButton getOccupant1(){ return occupantsNamesJB[1];}
	public JButton getOccupant2(){ return occupantsNamesJB[2];}
	public JButton getOccupant3(){ return occupantsNamesJB[3];}
	public JButton getOccupant4(){ return occupantsNamesJB[4];}
	public JButton getOccupant5(){ return occupantsNamesJB[5];}
	public JButton getOccupant6(){ return occupantsNamesJB[6];}
	public JButton getOccupant7(){ return occupantsNamesJB[7];}
}
