package rooms_and_permits_panels;

import java.awt.Dimension;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;

import jhunn_ate_visualManager.Utilities;

public class RoomsSquarePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JLabel roomsJL;
	private Utilities util;
	private JButton[] roomsJB;
	private JButton back;
	private String category;
	private int xRoom;
	private int yRoom;
	private int col;
	private int row;
	public RoomsSquarePanel(){
		category = new String("Student");
		setLayout(null);
		setOpaque(false);
		setBounds(0,0,1000,700);
		setPreferredSize(new Dimension(1000, 700));
		util = new Utilities();
		roomsJB = new JButton[12];
		xRoom = 100;
		yRoom = 50;
		col =0;
		row = 0;
		addButtons();
		addLabels();
	}
	public void addButtons(){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 20, 50, 50, this);
		for(int i =0; i < 12; i++){
			int j = i;
			j+=1;
			roomsJB[i] = new JButton("Room " + j );
		}
		for(int i = 0; i < 12; i ++){
			
			util.buttonProperties(roomsJB[i], xRoom, yRoom, 70, 100, this);
			col++;
			xRoom+= 100;
			if(col == 4){
				row++;
				yRoom +=100;
				xRoom = 100;
				col = 0;
			}
		}
		
		
	}
	public void addLabels(){
		roomsJL = new JLabel();
		util.labelProperties(roomsJL, 450, 30, 100, 100, "Rooms", this);
	}
	public void addListener(ActionListener listen){
		back.addActionListener(listen);
		for(int i = 0; i < 12; i++){
			roomsJB[i].addActionListener(listen);
			
		}
	}
	public JButton getBack(){ return back;}
	public JButton getRoom0(){ return roomsJB[0];}
	public JButton getRoom1(){ return roomsJB[1];}
	public JButton getRoom2(){ return roomsJB[2];}
	public JButton getRoom3(){ return roomsJB[3];}
	public JButton getRoom4(){ return roomsJB[4];}
	public JButton getRoom5(){ return roomsJB[5];}
	public JButton getRoom6(){ return roomsJB[6];}
	public JButton getRoom7(){ return roomsJB[7];}
	public JButton getRoom8(){ return roomsJB[8];}
	public JButton getRoom9(){ return roomsJB[9];}
	public JButton getRoom10(){ return roomsJB[10];}
	public JButton getRoom11(){ return roomsJB[11];}
	public String getCategory(){ return category;}

}
