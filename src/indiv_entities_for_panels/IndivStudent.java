package indiv_entities_for_panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class IndivStudent {
	//personal info
		private JLabel nameJL;
		private JLabel courseJL;
		private JLabel studentNoJL;
		private JLabel bdayJL;
		private JLabel contactNoJL;
		private JLabel stsBracketJL;
		private JLabel semStartedJL; //dormManager la it maedit
		private JLabel semEndedJL;
		private JLabel ayStartedJL;
		private JLabel ayEndedJL;
		private JLabel homeAddressJL;
		private JLabel emailAddressJL;
		private JLabel roomNumberJL;
		
		//parentsInfo
		private JLabel motherNameJL;
		private JLabel motherOccupationJL;
		private JLabel motherContactNoJL;
		private JLabel motherAddressJL;
		private JLabel fatherNameJL;
		private JLabel fatherOccupationJL;
		private JLabel fatherContactNoJL;
		private JLabel fatherAddressJL;
		//guardianInfo
		private JLabel guardianNameJL;
		private JLabel guardianOccupationJL;
		private JLabel guardianContactNoJL;
		private JLabel guardianAddressJL;
		private JLabel relationshipToGuardianJL;
		private JLabel userNameJL;
		private JLabel passwordJL;
		private Utilities util;
		private JLabel motherJL;
		private JLabel fatherJL;
		private JLabel guardianJL;
		private JLabel accountJL;
		private JLabel personalJL;
		
		private JTextField nameTF;
		private JTextField courseTF;
		private JTextField studentNoTF;
		private JTextField bdayTF;
		private JTextField contactNoTF;
		private JTextField stsBracketTF;
		private JTextField semStartedTF; //dormManager la it maedit
		private JTextField semEndedTF;
		private JTextField ayStartedTF;
		private JTextField ayEndedTF;
		private JTextField homeAddressTF;
		private JTextField emailAddressTF;
		private JTextField roomNumberTF;
		//parentsInfo
		private JTextField motherNameTF;
		private JTextField motherOccupationTF;
		private JTextField motherContactNoTF;
		private JTextField motherAddressTF;
		private JTextField fatherNameTF;
		private JTextField fatherOccupationTF;
		private JTextField fatherContactNoTF;
		private JTextField fatherAddressTF;
		//guardianInfo
		private JTextField guardianNameTF;
		private JTextField guardianOccupationTF;
		private JTextField guardianContactNoTF;
		private JTextField guardianAddressTF;
		private JTextField relationshipToGuardianTF;
		private JTextField userNameTF;
		private JTextField passwordTF;
	
		private JButton back;

		public IndivStudent(){
			util = new Utilities();
		}
		public void setLabels(JPanel panel){
			accountJL = new JLabel();
			util.labelProperties(accountJL, 700, 40, 250, 50, "Account Information", panel);
			personalJL = new JLabel();
			util.labelProperties(personalJL, 40, 40, 250, 50, "Personal Information", panel);
			nameJL = new JLabel();
			util.labelProperties(nameJL, 80, 70, 50, 50, "Name:", panel);
			courseJL = new JLabel();
			util.labelProperties(courseJL, 80, 100, 100, 50, "Course:", panel);
			studentNoJL = new JLabel();
			util.labelProperties(studentNoJL, 80, 130, 100, 50, "Student No:", panel);
			bdayJL = new JLabel();
			util.labelProperties(bdayJL, 80, 160, 100, 50, "Birthday:", panel);
			contactNoJL = new JLabel();
			util.labelProperties(contactNoJL, 80, 190, 100, 50, "Contact No:", panel);
			stsBracketJL = new JLabel();
			util.labelProperties(stsBracketJL, 80, 220, 100, 50, "StS Bracket:", panel);
			semStartedJL = new JLabel(); //dormManager la it maedit
			util.labelProperties(semStartedJL, 80, 250, 150, 50, "Semester Started:", panel);
			semEndedJL = new JLabel();
			util.labelProperties(semEndedJL, 80, 280, 150, 50, "Semester Ended:", panel);
			ayStartedJL = new JLabel();
			util.labelProperties(ayStartedJL, 80, 310, 150, 50, "AY Started:", panel);
			ayEndedJL = new JLabel();
			util.labelProperties(ayEndedJL, 80, 340, 100, 50, "AY Ended:", panel);
			homeAddressJL = new JLabel();
			util.labelProperties(homeAddressJL, 80, 370, 150, 50, "Home Address:", panel);
			emailAddressJL = new JLabel();
			util.labelProperties(emailAddressJL, 80, 400, 150, 50, "Email Address:", panel);
			roomNumberJL = new JLabel();
			util.labelProperties(roomNumberJL, 80, 430, 100, 50, "Room No:", panel);
		
			//parentsInfo
			motherJL = new JLabel();
			util.labelProperties(motherJL, 370, 40, 250, 50, "Mother's Information", panel);
			fatherJL = new JLabel();
			util.labelProperties(fatherJL, 370, 200, 250, 50, "Father's Information", panel);
			guardianJL = new JLabel();
			util.labelProperties(guardianJL, 370, 360, 250, 50, "Guardian's Information", panel);
			motherNameJL = new JLabel();
			util.labelProperties(motherNameJL, 410, 70, 100, 50, "Name:", panel);
			motherOccupationJL = new JLabel();
			util.labelProperties(motherOccupationJL, 410, 100, 100, 50, "Occupation:", panel);
			motherContactNoJL = new JLabel();
			util.labelProperties(motherContactNoJL, 410, 130, 100, 50, "Contact No:", panel);
			motherAddressJL = new JLabel();
			util.labelProperties(motherAddressJL, 410, 160, 100, 50, "Address:", panel);
			fatherNameJL = new JLabel();
			util.labelProperties(fatherNameJL, 410, 230, 100, 50, "Name:", panel);
			fatherOccupationJL = new JLabel();
			util.labelProperties(fatherOccupationJL, 410, 260, 100, 50, "Occupation:", panel);
			fatherContactNoJL = new JLabel();
			util.labelProperties(fatherContactNoJL, 410, 290, 100, 50, "Contact No:", panel);
			fatherAddressJL = new JLabel();
			util.labelProperties(fatherAddressJL, 410, 320, 100, 50, "Address:", panel);
			//guardianInfo
			guardianNameJL = new JLabel();
			util.labelProperties(guardianNameJL, 410, 390, 100, 50, "Name:", panel);
			guardianOccupationJL = new JLabel();
			util.labelProperties(guardianOccupationJL, 410, 420, 100, 50, "Occupation:", panel);
			guardianContactNoJL = new JLabel();
			util.labelProperties(guardianContactNoJL, 410, 450, 100, 50, "Contact No:", panel);
			guardianAddressJL = new JLabel();
			util.labelProperties(guardianAddressJL, 410, 480, 100, 50, "Address:", panel);
			relationshipToGuardianJL = new JLabel();
			util.labelProperties(relationshipToGuardianJL, 410, 510, 250, 50, "Relationship to Guardian:", panel);
			userNameJL = new JLabel();
			util.labelProperties(userNameJL, 740, 70, 100, 50, "Username:", panel);
			passwordJL = new JLabel();
			util.labelProperties(passwordJL, 740, 100, 100, 50, "Password:", panel);
			
		}
		public void setTextFields(JPanel panel){
			
			nameTF = new JTextField();
			util.textFieldProperties(nameTF, 130, 80, 240, 25, panel);
			courseTF = new JTextField();
			util.textFieldProperties(courseTF, 140, 110, 150, 25, panel);
			studentNoTF = new JTextField();
			util.textFieldProperties(studentNoTF, 170, 140, 150, 25, panel);
			bdayTF = new JTextField();
			util.textFieldProperties(bdayTF, 150, 170, 150, 25, panel);
			contactNoTF = new JTextField();
			util.textFieldProperties(contactNoTF, 170, 200, 150, 25, panel);
			stsBracketTF = new JTextField();
			util.textFieldProperties(stsBracketTF, 170, 230, 150, 25, panel);
			semStartedTF = new JTextField(); //dormManager la it maedit
			util.textFieldProperties(semStartedTF, 210, 260, 150, 25, panel);
			semEndedTF = new JTextField();
			util.textFieldProperties(semEndedTF, 210, 290, 150, 25, panel);
			ayStartedTF = new JTextField();
			util.textFieldProperties(ayStartedTF, 170, 320, 150, 25, panel);
			ayEndedTF = new JTextField();
			util.textFieldProperties(ayEndedTF, 170, 350, 150, 25, panel);
			homeAddressTF = new JTextField();
			util.textFieldProperties(homeAddressTF, 190, 380, 150, 25, panel);
			emailAddressTF = new JTextField();
			util.textFieldProperties(emailAddressTF, 190, 410, 150, 25, panel);
			roomNumberTF = new JTextField();
			util.textFieldProperties(roomNumberTF, 170, 440, 150, 25, panel);
		
			//it textarea it address
			
			motherNameTF = new JTextField();
			util.textFieldProperties(motherNameTF, 470, 80, 250, 25, panel);
			motherOccupationTF = new JTextField();
			util.textFieldProperties(motherOccupationTF, 490, 110, 170, 25, panel);
			motherContactNoTF = new JTextField();
			util.textFieldProperties(motherContactNoTF, 500, 140, 170, 25, panel);
			motherAddressTF = new JTextField();
			util.textFieldProperties(motherAddressTF, 490, 170, 250, 25, panel);
			fatherNameTF = new JTextField();
			util.textFieldProperties(fatherNameTF, 460, 240, 250, 25, panel);
			fatherOccupationTF = new JTextField();
			util.textFieldProperties(fatherOccupationTF, 500, 270, 180, 25, panel);
			fatherContactNoTF = new JTextField();
			util.textFieldProperties(fatherContactNoTF, 500, 300, 180, 25, panel);
			fatherAddressTF = new JTextField();
			util.textFieldProperties(fatherAddressTF, 480, 330, 250, 25, panel);
			//guardianInfo
			guardianNameTF = new JTextField();
			util.textFieldProperties(guardianNameTF, 460, 400, 250, 25, panel);
			guardianOccupationTF = new JTextField();
			util.textFieldProperties(guardianOccupationTF, 500, 430, 200, 25, panel);
			guardianContactNoTF = new JTextField();
			util.textFieldProperties(guardianContactNoTF, 500, 460, 180, 25, panel);
			guardianAddressTF = new JTextField();
			util.textFieldProperties(guardianAddressTF, 480, 490, 250, 25, panel);
			relationshipToGuardianTF = new JTextField();
			util.textFieldProperties(relationshipToGuardianTF, 600, 520, 200, 25, panel);
			userNameTF = new JTextField();
			util.textFieldProperties(userNameTF, 820, 80, 150, 25, panel);
			motherOccupationTF = new JTextField();
			passwordTF = new JTextField();
			util.textFieldProperties(passwordTF, 820, 110, 150, 25, panel);
		}
		public void setButtons(JPanel panel){
			back = new JButton("Back!");
			util.buttonProperties(back, 20, 10, 50, 30, panel);
		}
		public String getNameStr(){ return nameTF.getText();}
		public String getCourseStr(){ return courseTF.getText();}
		public String getStudentNoStr(){ return studentNoTF.getText();}
		public String getBdayStr(){ return bdayTF.getText();}
		public String getContactNoStr(){ return contactNoTF.getText();}
		public String getSTSBracketStr(){ return stsBracketTF.getText();}
		public String getSemStartedStr(){ return semStartedTF.getText();}
		public String getSemEndedStr(){ return semEndedTF.getText();}
		public String getAyStartedStr(){ return ayStartedTF.getText();}
		public String getAyendedStr(){ return ayEndedTF.getText();}
		public String getHomeAddStr(){ return homeAddressTF.getText();}
		public String getEmailAddStr(){ return emailAddressTF.getText();}
		public void setPersonalInfo(String name, String course, String sn, 
				String bday, String contact, String sts, String semStarted,
				String semEnded, String ayStarted, String ayEnded,
				String homeAdd, String emailAdd, String roomNum){
			nameTF.setText(name);
			courseTF.setText(course);
			studentNoTF.setText(sn);
			bdayTF.setText(bday);
			contactNoTF.setText(contact);
			stsBracketTF.setText(sts);
			semStartedTF.setText(semStarted); 
			semEndedTF.setText(semEnded);
			ayStartedTF.setText(ayStarted);
			ayEndedTF.setText(ayEnded);
			homeAddressTF.setText(homeAdd);
			emailAddressTF.setText(emailAdd);
			roomNumberTF.setText(roomNum);
		}
		public String getMotherNameStr(){ return motherNameTF.getText();}
		public String getMotherOccupationStr(){ return motherOccupationTF.getText();}
		public String getMotherContactNoStr(){ return motherContactNoTF.getText();}
		public String getMotherAddressStr(){ return motherAddressTF.getText();}
		
		public void setMotherInfo(String name, String occu, String contact,
				String add){
			motherNameTF.setText(name);
			motherOccupationTF.setText(occu);
			motherContactNoTF.setText(contact);
			motherAddressTF.setText(add);
		
		}
		
		public String getFatherNameStr(){ return fatherNameTF.getText();}
		public String getFatherOccupationStr(){ return fatherOccupationTF.getText();}
		public String getFatherContactNoStr(){ return fatherContactNoTF.getText();}
		public String getFatherAddressStr(){ return fatherAddressTF.getText();}
		
		public void setFatherInfo(String name, String occu, String contact,
				String add){
			fatherNameTF.setText(name);
			fatherOccupationTF.setText(occu);
			fatherContactNoTF.setText(contact);
			fatherAddressTF.setText(add);
		}
		public String getGuardianNameStr(){ return guardianNameTF.getText();}
		public String getGuardianOccupationStr(){ return guardianOccupationTF.getText();}
		public String getGuardianContactNoStr(){ return guardianContactNoTF.getText();}
		public String getGuardianAddressStr(){ return guardianAddressTF.getText();}
		public String getRelationshipToGuardianStr(){ return relationshipToGuardianTF.getText();}
		
		public void setGuardianInfo(String name, String occu, String contact,
				String add,String rel){
			guardianNameTF.setText(name);
			guardianOccupationTF.setText(occu);
			guardianContactNoTF.setText(contact);
			guardianAddressTF.setText(add);
			relationshipToGuardianTF.setText(rel);
		}
		public void setAccountInfo(String username, String pass){
			userNameTF.setText(username);
			passwordTF.setText(pass);
		}
		public void setIfEditable(boolean b){
			nameTF.setEditable(b);
			courseTF.setEditable(b);
			studentNoTF.setEditable(b);
			bdayTF.setEditable(b);
			contactNoTF.setEditable(b);
			stsBracketTF.setEditable(b);
			semStartedTF.setEditable(b); 
			semEndedTF.setEditable(b);
			ayStartedTF.setEditable(b);
			ayEndedTF.setEditable(b);
			homeAddressTF.setEditable(b);
			emailAddressTF.setEditable(b);
			roomNumberTF.setEditable(b);
			
			motherNameTF.setEditable(b);
			motherOccupationTF.setEditable(b);
			motherContactNoTF.setEditable(b);
			motherAddressTF.setEditable(b);
			
			fatherNameTF.setEditable(b);
			fatherOccupationTF.setEditable(b);
			fatherContactNoTF.setEditable(b);
			fatherAddressTF.setEditable(b);
			
			guardianNameTF.setEditable(b);
			guardianOccupationTF.setEditable(b);
			guardianContactNoTF.setEditable(b);
			guardianAddressTF.setEditable(b);
			relationshipToGuardianTF.setEditable(b);
			
			userNameTF.setEditable(b);
			passwordTF.setEditable(b);
			
			
		}
		public void setTFToNull(){
			nameTF.setText(null);
			courseTF.setText(null);
			studentNoTF.setText(null);
			bdayTF.setText(null);
			contactNoTF.setText(null);
			stsBracketTF.setText(null);
			semStartedTF.setText(null); 
			semEndedTF.setText(null);
			ayStartedTF.setText(null);
			ayEndedTF.setText(null);
			homeAddressTF.setText(null);
			emailAddressTF.setText(null);
			roomNumberTF.setText(null);
			
			motherNameTF.setText(null);
			motherOccupationTF.setText(null);
			motherContactNoTF.setText(null);
			motherAddressTF.setText(null);
			
			fatherNameTF.setText(null);
			fatherOccupationTF.setText(null);
			fatherContactNoTF.setText(null);
			fatherAddressTF.setText(null);
			
			guardianNameTF.setText(null);
			guardianOccupationTF.setText(null);
			guardianContactNoTF.setText(null);
			guardianAddressTF.setText(null);
			relationshipToGuardianTF.setText(null);
			
		}
		public void setTFHandler(ActionListener listen){
			nameTF.addActionListener(listen);
			courseTF.addActionListener(listen);
			studentNoTF.addActionListener(listen);
			bdayTF.addActionListener(listen);
			contactNoTF.addActionListener(listen);
			stsBracketTF.addActionListener(listen);
			semStartedTF.addActionListener(listen); 
			semEndedTF.addActionListener(listen);
			ayStartedTF.addActionListener(listen);
			ayEndedTF.addActionListener(listen);
			homeAddressTF.addActionListener(listen);
			emailAddressTF.addActionListener(listen);
			roomNumberTF.addActionListener(listen);
			
			motherNameTF.addActionListener(listen);
			motherOccupationTF.addActionListener(listen);
			motherContactNoTF.addActionListener(listen);
			motherAddressTF.addActionListener(listen);
			
			fatherNameTF.addActionListener(listen);
			fatherOccupationTF.addActionListener(listen);
			fatherContactNoTF.addActionListener(listen);
			fatherAddressTF.addActionListener(listen);
			
			guardianNameTF.addActionListener(listen);
			guardianOccupationTF.addActionListener(listen);
			guardianContactNoTF.addActionListener(listen);
			guardianAddressTF.addActionListener(listen);
			relationshipToGuardianTF.addActionListener(listen);
			
		}
		public JButton getBack(){return back;}
		
		public JTextField getNameTF(){	return nameTF;}
		public JTextField getHomeAddressTF(){	return homeAddressTF;}
		public JTextField getRoomNumberTF(){	return roomNumberTF;}
		
		public JTextField getMotherNameTF(){	return motherNameTF;}
		public JTextField getMotherOccupationTF(){	return motherOccupationTF;}
		public JTextField getMotherContactNoTF(){	return motherContactNoTF;}
		public JTextField getMotherAddressTF(){	return motherAddressTF;}
		
		public JTextField getFatherNameTF(){	return fatherNameTF;}
		public JTextField getFatherOccupationTF(){	return fatherOccupationTF;}
		public JTextField getFatherContactNoTF(){	return fatherContactNoTF;}
		public JTextField getFatherAddressTF(){	return fatherAddressTF;}
		
		public JTextField getGuardianNameTF(){	return guardianNameTF;}
		public JTextField getGuardianOccupationTF(){	return guardianOccupationTF;}
		public JTextField getGuardianContactNoTF(){	return guardianContactNoTF;}
		public JTextField getGuardianAddressTF(){	return guardianAddressTF;}
		public JTextField getRelationshipToGuaridanTF(){	return relationshipToGuardianTF;}
		
		public JTextField getUserNameTF(){	return userNameTF;}
		public JTextField getPasswordTF(){	return passwordTF;}
		
		public JTextField getCourseTF(){	return courseTF;}
		public JTextField getStudentNoTF(){	return studentNoTF;}
		public JTextField getContactNoTF(){	return contactNoTF;}
		public JTextField getSTSBracketTF(){	return stsBracketTF;}
		public JTextField getSemStartedTF(){	return semStartedTF;}
		public JTextField getSemEndedTF(){	return semEndedTF;}
		public JTextField getAYStartedTF(){	return ayStartedTF;}
		public JTextField getAYEndedTF(){	return ayEndedTF;}
		public JTextField getBdayTF(){	return bdayTF;}
		
}
