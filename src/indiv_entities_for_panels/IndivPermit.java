package indiv_entities_for_panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class IndivPermit {

	private JLabel categoryJL;
	private JLabel reasonJL;
	private JLabel placeJL;
	private JLabel nameJL;
	private JTextField nameTF;
	private JTextField categoryTF;
	private JTextField reasonTF;
	private JTextField placeTF;
	
	private JButton backJB;
	private Utilities util;
	public IndivPermit(){
		util = new Utilities();
	}
	public void setLabels(JPanel panel){

		nameJL = new JLabel();
		util.labelProperties(nameJL, 40, 70, 100, 50, "Name:", panel);
		categoryJL = new JLabel();
		util.labelProperties(categoryJL, 40, 100, 100, 50, "Category:", panel);
		reasonJL = new JLabel();
		util.labelProperties(reasonJL, 40, 130, 100, 50, "Reason:", panel);
		placeJL = new JLabel();
		util.labelProperties(placeJL, 40, 160, 100, 50, "Place:", panel);
		
	}
	public void setTextFields(JPanel panel){
		nameTF = new JTextField();
		util.textFieldProperties(nameTF, 120, 80, 150, 25, panel);
		categoryTF = new JTextField();
		util.textFieldProperties(categoryTF, 120, 120, 150, 25, panel);
		reasonTF = new JTextField();
		util.textFieldProperties(reasonTF, 120, 160, 150, 25, panel);
		placeTF = new JTextField();
		util.textFieldProperties(placeTF, 120, 200, 150, 25, panel);
		
	}
	public void setInfo(String name, String category, String reason, String place){
		nameTF.setText(name);
		categoryTF.setText(category);
		reasonTF.setText(reason);
		placeTF.setText(place);
	}
	public String getNameStr(){ return nameTF.getText();}
	public String getCategoryStr(){ return categoryTF.getText();}
	public String getReasonStr(){ return reasonTF.getText();}
	public String getPlaceStr(){ return placeTF.getText();}
	
	public void setIfEditable(boolean bol){
		nameTF.setEditable(bol);
		categoryTF.setEditable(bol);
		reasonTF.setEditable(bol);
		placeTF.setEditable(bol);
	}
	public void setButtons(JPanel panel){
		backJB = new JButton("Back");
		util.buttonProperties(backJB, 20, 440, 50, 60, panel);
	}
	public void setTFHandler(ActionListener listen){
		nameTF.addActionListener(listen);
		categoryTF.addActionListener(listen);
		reasonTF.addActionListener(listen);
		placeTF.addActionListener(listen);
	}
	public void setTFToNull(){
		nameTF.setText(null);
		categoryTF.setText(null);
		reasonTF.setText(null);
		placeTF.setText(null);
	}
	public JButton getBack(){ return backJB;}
	public JTextField getNameTF(){ return nameTF; }
	public JTextField getCategoryTF(){ return categoryTF; }
	public JTextField getReasonTF(){ return reasonTF; }
	public JTextField getPlaceTF(){ return placeTF;}
	
}
