package indiv_entities_for_panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class IndivGuard {
	//id_number, name,address,employer, duty_time, birthday,contact_number,status(Single,Married,Widowed)

	private Utilities util;
	private JLabel personalInfoJL;
	private JLabel workingInfoJL;
	private JLabel accountInfoJL;
	private JLabel nameJL; //
	private JLabel bdayJL; //
	private JLabel addressJL; //
	private JLabel contactNoJL; //
	private JLabel statusJL; //
	private JLabel userNameJL;
	private JLabel passwordJL;
	private JLabel employerJL; //
	private JLabel idNumberJL; //
	private JLabel dutyTimeJL; //
	private JButton back;
	private JTextField nameTF;
	private JTextField bdayTF;
	private JTextField addressTF;
	private JTextField contactNoTF;
	private JTextField statusTF;
	private JTextField userNameTF;
	private JTextField passwordTF;
	private JTextField employerTF;
	private JTextField idNumberTF;
	private JTextField dutyTimeTF;
	
	public IndivGuard(){
		util = new Utilities();
		
	}
	public void setLabels(JPanel panel){
		nameJL = new JLabel();
		bdayJL = new JLabel();
		addressJL = new JLabel();
		contactNoJL = new JLabel();
		statusJL = new JLabel();
		userNameJL = new JLabel();
		passwordJL = new JLabel();
		employerJL = new JLabel();
		idNumberJL = new JLabel();
		dutyTimeJL = new JLabel();
		personalInfoJL = new JLabel();
		workingInfoJL = new JLabel();
		accountInfoJL = new JLabel();
		util.labelProperties(personalInfoJL, 50, 20, 250, 30, "Personal Information", panel);
		util.labelProperties(nameJL, 80, 50, 100, 30, "Name:", panel);
		util.labelProperties(bdayJL, 80, 80, 100, 30, "Birthday:", panel);
		util.labelProperties(addressJL, 80, 110, 100, 30, "Address:", panel);
		util.labelProperties(contactNoJL, 80, 140, 150, 30, "Contact Number:", panel);
		util.labelProperties(statusJL, 80, 170, 100, 30, "Status:", panel);
		util.labelProperties(workingInfoJL, 50, 250, 250, 30, "Working Information", panel);
		util.labelProperties(employerJL, 80, 280, 100, 30, "Employer:", panel);
		util.labelProperties(idNumberJL, 80, 310, 100, 30, "ID Number:", panel);
		util.labelProperties(dutyTimeJL, 80, 340, 100, 30, "Duty Time:", panel);
		util.labelProperties(accountInfoJL, 50, 380, 250, 30, "Account Information", panel);
		util.labelProperties(userNameJL, 80, 410, 100, 30, "Username:", panel);
		util.labelProperties(passwordJL, 80, 440, 100, 30, "Password:", panel);
		
		
	}
	public void setButtons(JPanel panel){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 440, 50, 60, panel);
	}
	public void setListener(ActionListener listen){
		back.addActionListener(listen);
	}
	public void setTextFields(JPanel panel){
		nameTF = new JTextField();
		bdayTF = new JTextField();
		addressTF = new JTextField();
		contactNoTF = new JTextField();
		statusTF = new JTextField();
		userNameTF = new JTextField();
		passwordTF = new JTextField();
		employerTF = new JTextField();
		idNumberTF = new JTextField();
		dutyTimeTF = new JTextField();
		
		util.textFieldProperties(nameTF, 140, 50, 250, 25, panel);
		util.textFieldProperties(bdayTF, 160, 80, 150, 25, panel);
		util.textFieldProperties(addressTF, 160, 110, 250, 25, panel);
		util.textFieldProperties(contactNoTF, 200, 140, 150, 25, panel);
		util.textFieldProperties(statusTF, 140, 170, 150, 25, panel);
		util.textFieldProperties(userNameTF, 170,410, 150, 25, panel);
		util.textFieldProperties(passwordTF, 160, 440, 150, 25, panel);
		util.textFieldProperties(employerTF, 160, 280, 250, 25, panel);
		util.textFieldProperties(idNumberTF, 170, 310, 150, 25, panel);
		util.textFieldProperties(dutyTimeTF, 160, 340, 150, 25, panel);
		
		
	}
	public void setTFToNull(){
		nameTF.setText(null);
		bdayTF.setText(null);
		addressTF.setText(null);
		contactNoTF.setText(null);
		statusTF.setText(null);
		userNameTF.setText(null);
		passwordTF.setText(null);
		employerTF.setText(null);
		idNumberTF.setText(null);
		dutyTimeTF.setText(null);
	}
	public void setInfo(String name, String bday,String address, String contactNum,
			String status, String employer, String idNum, String dutyTime, String username,
			String password){
		nameTF.setText(name);
		bdayTF.setText(bday);
		addressTF.setText(address);
		contactNoTF.setText(contactNum);
		statusTF.setText(status);
		userNameTF.setText(username);
		passwordTF.setText(password);
		employerTF.setText(employer);
		idNumberTF.setText(idNum);
		dutyTimeTF.setText(dutyTime);
	}
	public String getNameStr(){ return nameTF.getText();}
	public String getBdayStr(){ return bdayTF.getText();}
	public String getAddressStr(){ return addressTF.getText();}
	public String getContactNoStr(){ return contactNoTF.getText();}
	public String getStatusStr(){ return statusTF.getText();}
	public String getUsernameStr(){ return userNameTF.getText();}
	public String getPasswordStr(){ return passwordTF.getText();}
	public String getEmployerStr(){ return employerTF.getText();}
	public String getIDNumStr(){ return idNumberTF.getText();}
	public String getDutyTimeStr(){ return dutyTimeTF.getText();}
	
	public void setIfEditable(boolean bol){
		nameTF.setEditable(bol);
		bdayTF.setEditable(bol);
		addressTF.setEditable(bol);
		contactNoTF.setEditable(bol);
		statusTF.setEditable(bol);
		userNameTF.setEditable(bol);
		passwordTF.setEditable(bol);
		employerTF.setEditable(bol);
		idNumberTF.setEditable(bol);
		dutyTimeTF.setEditable(bol);
	}
	
	public void setTFListener(ActionListener listen){
		getNameTF().addActionListener(listen);
		getBdayTF().addActionListener(listen);
		getAddressTF().addActionListener(listen);
		getContactNoTF().addActionListener(listen);
		getStatusTF().addActionListener(listen);
		getUserNameTF().addActionListener(listen);
		getPasswordTF().addActionListener(listen);
		getEmployerTF().addActionListener(listen);
		getidNumberTF().addActionListener(listen);
		getDutyTimeTF().addActionListener(listen);
	}
	public JButton getBack(){ return back;}
	public JTextField getNameTF(){return nameTF;}
	public JTextField getBdayTF(){return bdayTF;}
	public JTextField getAddressTF(){return addressTF;}
	public JTextField getContactNoTF(){return contactNoTF;}
	public JTextField getStatusTF(){return statusTF;}
	public JTextField getUserNameTF(){return userNameTF;}
	public JTextField getPasswordTF(){return passwordTF;}
	public JTextField getEmployerTF(){return employerTF;}
	public JTextField getidNumberTF(){return idNumberTF;}
	public JTextField getDutyTimeTF(){return dutyTimeTF;}
	
	
}
