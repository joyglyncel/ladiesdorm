package indiv_entities_for_panels;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import jhunn_ate_visualManager.Utilities;

public class IndivDormMgr{

	private Utilities util;
	private JLabel nameJL;
	private JLabel bdayJL;
	private JLabel addressJL;
	private JLabel contactNoJL;
	private JLabel statusJL;
	private JLabel userNameJL;
	private JLabel passwordJL;
	private JButton back;
	private JTextField nameTF;
	private JTextField bdayTF;
	private JTextField addressTF;
	private JTextField contactNoTF;
	private JTextField statusTF;
	private JTextField userNameTF;
	private JTextField passwordTF;
	public IndivDormMgr(){
		util = new Utilities();
		
	}
	public void setLabels(JPanel panel){
		nameJL = new JLabel();
		bdayJL = new JLabel();
		addressJL = new JLabel();
		contactNoJL = new JLabel();
		statusJL = new JLabel();
		userNameJL = new JLabel();
		passwordJL = new JLabel();
		
		util.labelProperties(nameJL, 50, 50, 100, 50, "Name:", panel);
		util.labelProperties(bdayJL, 50, 80, 100, 50, "Birthday:", panel);
		util.labelProperties(addressJL, 50, 110, 100, 50, "Address:", panel);
		util.labelProperties(contactNoJL, 50, 140, 150, 50, "Contact Number:", panel);
		util.labelProperties(statusJL, 50, 170, 100, 50, "Status:", panel);
		util.labelProperties(userNameJL, 50, 230, 100, 50, "Username:", panel);
		util.labelProperties(passwordJL, 50, 260, 100, 50, "Password:", panel);
		
	}
	public void setTextFields(JPanel panel){
		nameTF = new JTextField();
		bdayTF = new JTextField();
		addressTF = new JTextField();
		contactNoTF = new JTextField();
		statusTF = new JTextField();
		userNameTF = new JTextField();
		passwordTF = new JTextField();
		util.textFieldProperties(nameTF, 130, 60, 150, 25, panel);
		util.textFieldProperties(bdayTF, 130, 90, 150, 25, panel);
		util.textFieldProperties(addressTF, 130, 120, 150, 25, panel);
		util.textFieldProperties(contactNoTF, 180, 150, 150, 25, panel);
		util.textFieldProperties(statusTF, 130, 180, 150, 25, panel);
		util.textFieldProperties(userNameTF, 130, 240, 150, 25, panel);
		util.textFieldProperties(passwordTF, 130, 270, 150, 25, panel);
	}
	public void setInfo(String name, String bday, String address, String contactNum, 
			String status, String username, String password){
		nameTF.setText(name);
		bdayTF.setText(bday);
		addressTF.setText(address);
		contactNoTF.setText(contactNum);
		statusTF.setText(status);
		userNameTF.setText(username);
		passwordTF.setText(password);
	}
	public void setTFToNull(){
		nameTF.setText(null);
		bdayTF.setText(null);
		addressTF.setText(null);
		contactNoTF.setText(null);
		statusTF.setText(null);
		userNameTF.setText(null);
		passwordTF.setText(null);
	}
	public String getNameStr(){ return nameTF.getText();}
	public String getBdayStr(){ return bdayTF.getText();}
	public String getAddressStr(){ return addressTF.getText();}
	public String getContactNumStr(){ return contactNoTF.getText();}
	public String getStatusStr(){ return statusTF.getText();}
	public String getUsernameStr(){ return userNameTF.getText();}
	public String getPasswordStr(){ return passwordTF.getText();}
	
	public void setIfEditable(boolean bol){
		nameTF.setEditable(bol);
		bdayTF.setEditable(bol);
		addressTF.setEditable(bol);
		contactNoTF.setEditable(bol);
		statusTF.setEditable(bol);
		userNameTF.setEditable(bol);
		passwordTF.setEditable(bol);
	}
	public void setButtons(JPanel panel){
		back = new JButton("Back");
		util.buttonProperties(back, 20, 440, 50, 60, panel);
	}
	public void addTFListener(ActionListener listen){
		getNameTF().addActionListener(listen);
		getbdayTF().addActionListener(listen);
		getAddressTF().addActionListener(listen);
		getContactNoTF().addActionListener(listen);
		getStatusTF().addActionListener(listen);
		getUserNameTF().addActionListener(listen);
		getPasswordTF().addActionListener(listen);

	}
	public JButton getBack(){ return back;}
	public JTextField getNameTF(){ return nameTF;}
	public JTextField getbdayTF(){ return bdayTF;}
	public JTextField getAddressTF(){ return addressTF;}
	public JTextField getContactNoTF(){ return contactNoTF;}
	public JTextField getStatusTF(){ return statusTF;}
	public JTextField getUserNameTF(){ return userNameTF;}
	public JTextField getPasswordTF(){ return passwordTF;}

	
	
}
