package jhunn_mysqlStuff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectorMain {
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost/LadiesDorm";
	private static final String USER = "root";
	private static final String PASS = "april";
	private Connection conn = null;
	private Statement stmt = null;
	public ConnectorMain(){
		try{
		      Class.forName(JDBC_DRIVER);
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);
		      
		}catch(SQLException se){
		      se.printStackTrace();
		}catch(Exception e){
		      e.printStackTrace();
		}
	}
	
	public void executeUpdate( String command){
		//insert, update, delete
		try {
			System.out.println(command);
			stmt = conn.createStatement();
			int value = stmt.executeUpdate(command);
			System.out.println(command + "has been executed");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void executeStudentGeneralQuery(String str){
		String name = new String();
		String student_number = new String();
		String course = new String();
		String sts_bracket = new String();
		String home_add = new String();
		String birthday = new String();
		String email_add = new String();
		String room_num = new String();
		String contact_num = new String();
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(str);
		      while(rs.next()){
		    	 System.out.print("| ");
		         
		         name = rs.getString("name");
		 		 student_number =  rs.getString("student_number");
		 		 course =  rs.getString("course");
		 		 sts_bracket =  rs.getString("sts_bracket");
		 		 home_add =  rs.getString("home_add");
		 		 birthday = rs.getString("birthday");
		 		 email_add =  rs.getString("email_add");
		 		 room_num =  rs.getString("room_num");
		 		 contact_num = rs.getString("contact_num");
		 		 
		         System.out.print(name);
		         numSpaces(13-name.length());
		         System.out.print("| "+student_number);
		         System.out.print("| "+ contact_num);
		         numSpaces(6-course.length());
		         System.out.println("|");
		      }
		      rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void executeGuardGeneralQuery(String str){
		
		String name = new String();
		String id_number = new String();
		String address = new String();
		String employer = new String();
		String duty_time = new String();
		String birthday = new String();
		String contact_num = new String();
		String status = new String();
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(str);
		      while(rs.next()){
		    	 System.out.print("| ");
		         
		         name = rs.getString("name");
		         id_number = rs.getString("id_number");
		         address =rs.getString("address");
		         employer =rs.getString("employer");
		         duty_time =rs.getString("duty_time");
		 		 birthday = rs.getString("birthday");
		 		 contact_num = rs.getString("contact_num");
		 		 status = rs.getString("status");
		 		 
		 		 //ibabalyu hin text fields
		         System.out.print(name);
		         numSpaces(13-name.length());
		         System.out.print("| "+id_number);
		         System.out.print("| "+ address);
		         numSpaces(6-address.length());
		         System.out.println("|");
		      }
		      rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
public void executeDormManagerGeneralQuery(String str){
	//statuss(Single, Married, Widowed)
		String name = new String();
		String id_number = new String();
		String address = new String();
		String birthday = new String();
		String contact_number = new String();
		String status = new String();
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(str);
		      while(rs.next()){
		    	 System.out.print("| ");
		         
		         name = rs.getString("name");
		         id_number = rs.getString("id_number");
		         address =rs.getString("address");
		 		 birthday = rs.getString("birthday");
		 		 contact_number = rs.getString("contact_num");
		 		 status = rs.getString("status");
		 		 
		 		 //ibabalyu hin text fields
		         System.out.print(name);
		         numSpaces(13-name.length());
		         System.out.print("| "+id_number);
		         System.out.print("| "+ address);
		         numSpaces(6-address.length());
		         System.out.println("|");
		      }
		      rs.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	public void numSpaces(int num){
		for(int i = 0; i < num; i++){
			System.out.print(" ");
		}
	}
	public void closeConnector(){
		
	     try{
	         if(stmt!=null)
		            stmt.close();
	     }catch(SQLException se2){
		    	  se2.printStackTrace();
	     }
	     try{
	    	 if(conn!=null)
	    		 conn.close();
	     }catch(SQLException se){
	    	 se.printStackTrace();
	     }
		
	}
}
