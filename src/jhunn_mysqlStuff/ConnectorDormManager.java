package jhunn_mysqlStuff;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectorDormManager {
	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";  
	private static final String DB_URL = "jdbc:mysql://localhost/LadiesDorm";
	private static final String USER = "root";
	private static final String PASS = "april";
	private Connection conn = null;
	private Statement stmt = null;
	public ConnectorDormManager(){
		try{
		      Class.forName(JDBC_DRIVER);
		      System.out.println("Connecting to database...");
		      conn = DriverManager.getConnection(DB_URL,USER,PASS);

		}catch(SQLException se){
		      se.printStackTrace();
		}catch(Exception e){
		      e.printStackTrace();
		}
	}
	
	public void executeUpdate( String command){
		//insert, update, delete 
		//ipapasa nala hit class nga responsible hit
		try {
			System.out.println(command);
			stmt = conn.createStatement();
			int value = stmt.executeUpdate(command);
			System.out.println(command + "has been executed");
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	

	public void closeConnector(){
		
	     try{
	         if(stmt!=null)
		            stmt.close();
	     }catch(SQLException se2){
		    	  se2.printStackTrace();
	     }
	     try{
	    	 if(conn!=null)
	    		 conn.close();
	     }catch(SQLException se){
	    	 se.printStackTrace();
	     }
		
	}
}
